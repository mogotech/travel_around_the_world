$(function () {
    FastClick.attach(document.body);

    //购买資產的弹出框
    $(".buyproduct").click(function () {
        var hidid = $("#hidid").val();
        // $(".select-dlog").css('display', 'block');
        // $('body').css({
        //     "overflow-x": "hidden",
        //     "overflow-y": "hidden"
        // });
        //阻止默认
        // $("body").on("touchmove", function (event) {
        //     event.preventDefault;
        // }, false);
        // $(".buybtn").click(function () {
            // $(".select-dlog").css('display', 'none');
            // $(".buy-dlog").css('display', 'block');
            $.ajax({
                type: "POST",
                url: "/index.php/world/goods/ajax_data",
                dataType: "json",
                data: {
                    id: $("#hidid").val()
                },
                success: function (data) {
                    var allnum = data.content.sum;
                    var shouxu = data.content.con.purchase_fee;
                    var danjia = data.content.goods.money;
                    var zong = eval(danjia*shouxu)+eval(danjia);
                    var c_s = eval(danjia*shouxu).toFixed(2);
                    $('#shouxu').html(c_s);
                    $('#zong').html(zong.toFixed(2));
                    $('.numInput').bind('input propertychange', function() {
                        var shouxu = data.content.con.purchase_fee;
                        var danjia = data.content.goods.money;
                        var num = $('.numInput').val();
                        var z_shouxu = eval(shouxu * danjia * num).toFixed(2);
                        var zong = eval(z_shouxu)+eval(danjia*num);
                        var z_zong = eval(zong).toFixed(2);
                        $('#shouxu').html(z_shouxu);
                        $('#zong').html(z_zong);
                    });
                    $(".buybtn").click(function (e) {
                        if ($('.numInput').val() < 1) {
                            e.preventDefault();
                            $.alert("輸入的數量不能小於1!");
                        } else {
                            $.confirm({
                                title: '提示',
                                text: '確定買入嗎？',
                                onOK: function () {
                                    //点击确认
                                    $.ajax({
                                        type: "POST",
                                        url: "/index.php/world/goods/order",
                                        dataType: "json",
                                        data: {
                                            id: $("#hiddid").val(),
                                            sum: $('.numInput').val(),
                                            buy_type: $("input[name='buy_type']:checked").val()
                                        },
                                        success: function (data) {
                                            $.alert(data.msg + "<br/>" + data.msg1, function () {
                                                $(".buy-dlog").css('display', 'none');
                                                $('body').css({
                                                    "overflow-x": "auto",
                                                    "overflow-y": "auto"
                                                });
                                                //然后点击取消或者确定时再取消body上的绑定
                                                $("body").off("touchmove");
                                            });
                                        }
                                    })
                                }
                            });
                        }
                    })
                }
            })
        // })
        $(".buy-dlog").css('display', 'block');
        $('body').css({
            "overflow-x": "hidden",
            "overflow-y": "hidden"
        });
        //阻止默认
        $("body").on("touchmove", function (event) {
            event.preventDefault;
        }, false);
    });

    $('.buy-close').click(function () {
        $(".buy-dlog").css('display', 'none');
        $(".select-dlog").css('display', 'none');
        $('body').css({
            "overflow-x": "auto",
            "overflow-y": "auto"
        });
        //然后点击取消或者确定时再取消body上的绑定
        $("body").off("touchmove");
        $('.numInput').val(1);
    });

});

// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init($('#chart')[0]);
var xsarea;
$.ajax({
    type: "POST",
    url: "/index.php/world/goods/main_ajax",
    dataType: "json",
    data: {
        id: $("#hidid").val()
    },
    success: function (data) {
        var data0 = [];
        var option, conlen;
        if (data.content != null) {
            conlen = data.content.length;
        } else {
            conlen = 0;
        }
        for (var i = 0; i < conlen; i++) {
            var shuju = [];
            shuju.push(data.content[i].time);
            shuju.push(data.content[i].start_money);
            shuju.push(data.content[i].end_money);
            shuju.push(data.content[i].highest_money);
            shuju.push(data.content[i].minimum_money);
            data0.push(shuju);
        }

        // 数据意义：s时间, 开盘(open)，收盘(close)，最低(lowest)，最高(highest)
        data0 = splitData(data0);

        function splitData(rawData) {
            var categoryData = [];
            var values = [];
            for (var i = 0; i < rawData.length; i++) {
                categoryData.push(rawData[i].splice(0, 1)[0]);
                values.push(rawData[i])
            }
            var data0len = rawData.length;
            var xslen = (data0len - 11) / data0len;
            xsarea = xslen * 100;
            return {
                categoryData: categoryData,
                values: values
            };
        }

        function calculateMA(dayCount) {
            var result = [];
            for (var i = 0, len = data0.values.length; i < len; i++) {
                if (i < dayCount) {
                    result.push('-');
                    continue;
                }
                var sum = 0;
                for (var j = 0; j < dayCount; j++) {
                    var newval = data0.values[i - j][1];
                    newval = parseFloat(newval);
                    sum += newval;
                }
                result.push(sum / dayCount);
            }
            return result;
        }

        option = {
            backgroundColor: '#21202D',
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    animation: false,
                    type: 'cross',
                    lineStyle: {
                        color: '#376df4',
                        width: 2,
                        opacity: 1
                    }
                }
            },
            legend: {
                data: ['價格節點', 'MA5', 'MA10', 'MA20'],
                inactiveColor: '#777',
                textStyle: {
                    color: '#ffffff'
                }
            },
            xAxis: {
                type: 'category',
                data: data0.categoryData,
                axisLine: {lineStyle: {color: '#C1C1C1'}}
            },
            yAxis: {
                scale: true,
                splitArea: {
                    show: false //坐标轴在 grid 区域中的分隔区域,默认不显示
                },
                splitLine: {
                    show: false //坐标轴在 grid 区域中的分隔线。默认数值轴显示，类目轴不显示。
                },
                axisTick: {
                    inside: true
                },
                axisLabel: {
                    inside: true
                },
                axisLine: {lineStyle: {color: '#C1C1C1'}},
            },
            dataZoom: [
                {
                    type: 'inside',
                    //start: xsarea,
                    start: 0,
                    end: 100
                },
                {
                    textStyle: {
                        color: '#8392A5'
                    },
                    handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                    handleSize: '80%',
                    dataBackground: {
                        areaStyle: {
                            color: '#8392A5'
                        },
                        lineStyle: {
                            opacity: 0.8,
                            color: '#8392A5'
                        }
                    },
                    handleStyle: {
                        color: '#fff',
                        shadowBlur: 3,
                        shadowColor: 'rgba(0, 0, 0, 0.6)',
                        shadowOffsetX: 2,
                        shadowOffsetY: 2
                    }
                }
            ],
            series: [
                {
                    name: "價格節點",
                    type: 'candlestick',
                    data: data0.values,
                    dimensions: ['date', '開盤價', '收盤價', '最高價', '最低價'],
                    itemStyle: {
                        normal: {
                            color: '#FD1050',
                            color0: '#0CF49B',
                            borderColor: '#FD1050',
                            borderColor0: '#0CF49B'
                        }
                    }
                },
                {
                    name: 'MA5',
                    type: 'line',
                    data: calculateMA(5),
                    smooth: true,
                    showSymbol: false,
                    lineStyle: {
                        normal: {
                            width: 1
                        }
                    }
                },
                {
                    name: 'MA10',
                    type: 'line',
                    data: calculateMA(10),
                    smooth: true,
                    showSymbol: false,
                    lineStyle: {
                        normal: {
                            width: 1
                        }
                    }
                },
                {
                    name: 'MA20',
                    type: 'line',
                    data: calculateMA(20),
                    smooth: true,
                    showSymbol: false,
                    lineStyle: {
                        normal: {
                            width: 1
                        }
                    }
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }
});