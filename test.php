<?php
/**
 * Created by PhpStorm.
 * User: ou
 * Date: 2018/4/20
 * Time: 17:48
 */
//先删除目录下的文件：
$dir= $_SERVER['DOCUMENT_ROOT'].'/runtime';

function deldir($dir)
{
    $dh = opendir($dir);
    while ($file = readdir($dh))
    {
        if ($file != "." && $file != "..")
        {
            $fullpath = $dir . "/" . $file;
            if (!is_dir($fullpath))
            {
                unlink($fullpath);
            } else
            {
                deldir($fullpath);
            }
        }
    }
    closedir($dh);
    if (rmdir($dir))
    {
        return true;
    } else
    {
        return false;
    }
}
deldir($dir); // e:/test/aaa 是你要删除的文件夹