<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：config.php
 * 时间：2017年8月25日
 * 作者：
 */

/**
 * Created by PhpStorm.
 * User: 
 * Date: 2017年8月25日
 * Time: 15点01分
 */

namespace app\world\admin;
use app\admin\controller\Admin;


class Config extends Admin
{
    /**
     * 初始化模块
     * @return mixed
     */
    public function index()
    {
        // 调用ModuleConfig()方法即可
        return $this->moduleConfig();
    }
}