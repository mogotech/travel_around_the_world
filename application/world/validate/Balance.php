<?php
/**
 * Created by PhpStorm.
 * User: ou
 * Date: 2018/3/13
 * Time: 9:55
 */
namespace app\world\validate;

use think\Validate;

class Balance extends Validate
{
    //定义验证规则
    protected $rule = [
        'balance_rate_1|C级结算商兑入率' => 'require|number|between:0.00,1.00',
        'balance_rate_2|B级结算商兑入率' => 'require|number|between:0.00,1.00',
        'balance_rate_3|A级结算商兑入率' => 'require|number|between:0.00,1.00',

    ];
    //定义验证提示
    protected $message = [
        'balance_rate_1.require' => 'C级结算商兑入率必须填写',
        'balance_rate_1.number' => 'C级结算商兑入率在0.00-1.00之间',
        'balance_rate_2.require' => 'B级结算商兑入率必须填写',
        'balance_rate_2.number' => 'B级结算商兑入率在0.00-1.00之间',
        'balance_rate_3.require' => 'A级结算商兑入率必须填写',
        'balance_rate_3.number' => 'A级结算商兑入率在0.00-1.00之间',
    ];


}