<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;

class GoodsRuls extends Validate
{
    //定义验证规则
    protected $rule = [
        'money|金额' => 'require|number',
        'time|时间' => 'require',
        'times|时间' => 'require',
    ];




    //定义验证场景
    protected $scene = [
        'admin_add' => ['money', 'time'],
        'admin_edit' => ['money', 'time'],
        'home_add' => ['money', 'times'],
    ];

}
