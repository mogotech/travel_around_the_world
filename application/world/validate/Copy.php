<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;
use think\Validate;
use think\db;


class Companyuser extends Validate
{
    //定义验证规则
    protected $rule = [
        'name|公司名称' => 'require',
        'tel|手机号'   => 'regex:^1\d{10}|unique:logistics_companyuser|require',
        'password|密码'  => 'require|length:6,20',
        'yzm|验证码' =>'require',
    ];

    //定义验证提示
    protected $message = [
        'name.require'          =>'公司名称不能为空',
        'mobile.regex'          => '手机号不正确',
        'mobile.unique'         => '手机号已存在',
        'mobile.require'        => '请填写手机号码',
        'password.require'      => '密码不能为空',
        'password.length'       => '密码长度6-20位',
    ];



    //定义验证场景
    protected $scene = [
        //更新
        'admin_create'=>['tel','password'],
        'admin_update'  =>  ['tel'],
        'home_create'=>['tel','password','yzm'],
        'update_password' => ['password','new_password']
    ];

}
