<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;
use think\db;


class Apply extends Validate
{
    //定义验证规则
    protected $rule = [
        'type|分类' => 'require',
        'content|内容' => 'require',
    ];




    //定义验证场景
    protected $scene = [
        'add' => ['type', 'content'],
    ];

}
