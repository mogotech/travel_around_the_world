<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;

class Goods extends Validate
{
    //定义验证规则
    protected $rule = [
        'name|資產名称' => 'require',
        'int_money|初始金额' => 'require|number|egt:0',
        'section|价格区间' => 'require|number|gt:0',
        'shop_min|最小成交金额' => 'number',
        'shop_max|最小成交量' => 'number',
    ];


    //定义验证场景
    protected $scene = [
        'add' => ['name', 'int_money', 'section',],
        'edit' => ['section',],
    ];

}
