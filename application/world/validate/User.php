<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：User.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;


class User extends Validate
{
    //定义验证规则
    protected $rule = [
        'username|账号' => 'require|unique:world_user|length:6,11',
        'nickname|昵称' => 'require',
        'password1|密码' => 'require|length:6,20',
        'password2|密码' => 'length:6,20',
        'money1|余额' => 'number',
        'withdrawal_type|兑出方式' => 'require|checkIs:a',
        'account|兑出账号' => 'require',
        'account_name|兑出姓名' => 'require',
        'bank|开户银行' => 'require',
        'acc_bank|开户支行' => 'require',
        'get_money|充值余额' => 'number|checkEdit:a',


    ];

    //定义验证提示
    protected $message = [
        'username.require' => '请输入账号',
        'username.unique' => '账号已存在',
        'username.length' => '账号长度6-11位',
        'password.require' => '请输入密码',
        'password.length' => '密码长度6-11位',
    ];


    //定义验证场景
    protected $scene = [
        'admin_add' => ['username', 'password1'],
        'home_add' => ['username', 'password1'],
        'home_edit' => ['password' => 'length:6,20'],
        'admin_edit' => ['get_money', 'password1' => 'length:6,20', 'password2' => 'length:6,20'],
        'login' => ['username', 'password1'],
        'home_add2' => ['username', 'nickname', 'password1', 'withdrawal_type', 'account', 'account_name'],
        'home_edit2' => ['username', 'nickname', 'password1' => 'length:6,20', 'withdrawal_type', 'account', 'account_name'],
        'home_edit3' => ['withdrawal_type', 'account', 'account_name'],
    ];


    protected function checkIs($value, $rule, $data)
    {
        if ($value == 1) {
            if (!$data['bank']) {
                return '银行卡兑出请填写开户银行和开户支行';
            }
            return true;
        } else {
            if ($data['bank'] || $data['acc_bank']) {
                return '非银行卡兑出请不要填写开户银行和开户支行';
            }
            return true;
        }
    }


    protected function checkEdit($value, $rule, $data)
    {
        if (isset($data['edit_money'])) {
            return true;
        }
        return '请选择充值类型';

    }


}
