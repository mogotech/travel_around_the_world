<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;


class Configs extends Validate
{
    //定义验证规则
    protected $rule = [
        'start_time|开市时间' => 'require',
        'end_time|闭市时间' => 'require',
        'electronics_wallet|电子钱包' => 'require|number|between:0.00,1.00',
        'purchase_fee|购买资产手续费' => 'require|number|between:0.00,1.00',
        'sell_fee|卖出资产手续费' => 'require|number|between:0.00,1.00',
        'exchange|美金汇率' => 'require|number|between:0.00,100.00',
        'fee_money|提现手续费' => 'require|number|between:0.00,1.00',
        'commission1|一级佣金' => 'require|number|between:0.00,1.00',
        'commission2|二级佣金' => 'require|number|between:0.00,1.00',
        'commission3|三级佣金' => 'require|number|between:0.00,1.00',
        'commission4|四级佣金' => 'require|number|between:0.00,1.00',
        'commission5|五级佣金' => 'require|number|between:0.00,1.00',
        'commission6|六级佣金' => 'require|number|between:0.00,1.00',
        'commission7|七级佣金' => 'require|number|between:0.00,1.00',
        'sum_commission1|一级佣金' => 'require|number',
        'sum_commission2|二级佣金' => 'require|number',
        'sum_commission3|三级佣金' => 'require|number',
        'sum_commission4|四级佣金' => 'require|number',
        'sum_commission5|五级佣金' => 'require|number',
        'sum_commission6|六级佣金' => 'require|number',
        'sum_commission7|七级佣金' => 'require|number',
        'sum_money1|一级总资产' => 'require|number',
        'sum_money2|二级总资产' => 'require|number',
        'sum_money3|三级总资产' => 'require|number',
        'sum_money4|四级总资产' => 'require|number',
        'sum_money5|五级总资产' => 'require|number',
        'sum_money6|六级总资产' => 'require|number',
        'sum_money7|七级总资产' => 'require|number',
        'overdue|逾期天数' => 'require|number',
        'mortgage_top|抵押资产价格上限' => 'require|number',
//        'brokers|同等级经纪商比例' => 'require|number|between:0.00,1.00',
//        'brokers1|区域经济商' => 'require|number|between:0.00,1.00',
//        'brokers2|大区经纪商' => 'require|number|between:0.00,1.00',
//        'brokers3|国际经纪商' => 'require|number|between:0.00,1.00',
        'margin|补仓线' => 'require|number',
        'selling|平仓线' => 'require|number',
        'capproportion1|配资比例' => 'number',
        'capproportion2|配资比例' => 'number',
        'capproportion3|配资比例' => 'number',
        'capproportion4|配资比例' => 'number',
        'capproportion5|配资比例' => 'number',
    ];


}
