<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\validate;

use think\Validate;

class Brokers extends Validate
{
    //定义验证规则
    protected $rule = [
        'brokers|同等级经纪商比例' => 'require|number',
        'brokers1|区域经纪商' => 'require|number',
        'brokers2|大区经纪商' => 'require|number',
        'brokers3|国际经纪商' => 'require|number',
    ];

    //定义验证场景
    protected $scene = [
        //更新
        'update' => ['brokers', 'brokers1', 'brokers2', 'brokers3']
    ];

}
