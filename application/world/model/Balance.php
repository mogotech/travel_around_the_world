<?php
/**
 * Created by PhpStorm.
 * User: ou
 * Date: 2018/3/13
 * Time: 10:06
 */
namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use app\world\model\User;
class Balance extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_balance';
    public function update_balance($data=''){
        return self::update($data);
    }
    public function find_balance(){

       return self::where('id','=',1)->find();

    }

    /**
     * 获取结算商等级(中文)
     * @param $level   结算商等级
     * @return array|int|mixed $_SESSION['is_language']
     */
    public function get_balance_level($level){

        $arr=['C級結算商','B級結算商','A級結算商'];
        $arr1=['C level settlement','B level settlement','A level settlement'];
        if(@$_SESSION['is_language']==1){
            $data=$arr;
        }else{
            $data=$arr1;
        }
        if(is_array($level)){
            if(count($level) == count($level, 1)){//一维数组
                if($level['balance_level']!=0){
                    $level['balance_level']=$data[$level['balance_level']-1];
                }
            }else{//二维数组
                foreach ($level as $key=>$value){
                    if($value['balance_level']!=0){
                        $level[$key]['balance_level']=$data[$value['balance_level']-1];
                    }
                }
            }
        }else{
            if($level==0) return 0;
            $level= $data[$level-1];
        }
        return $level;
    }

    /**
     * 查找上级结算商
     * @param $user
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function get_father_balance($user){


        $father_user=User::where('id','=',$user['pid'])->field('id,pid,group_id,balance_level')->find();
        $father_user=$this->get_father_balance1($user,$father_user);
        return $father_user;

    }
    public function get_father_balance1($user,$father_user){
        if($father_user['pid']==0){
            return $father_user;
        }
        $ffather_user=User::where('id','=',$father_user['pid'])->field('id,pid,group_id,balance_level')->find();

        if($father_user['balance_level']>$user['balance_level']) return $father_user;
        return $this->get_father_balance1($user,$ffather_user);

//        if($user['balance_level']==0){//普通用户
//            if($father_user['balance_level']==0){
//
//                return $this->get_father_balance1($user,$ffather_user);
//            }
//            return $father_user;
//        }else{//结算商
//
//            if($father_user['balance_level']<$user['balance_level']||$father_user['balance_level']==0){
//
//                return $this->get_father_balance1($user,$ffather_user);
//            }
//            return $father_user;
//        }
    }

    /**
     * @return mixed
     */
    public function get_user_balance_msg(){
        $User=new User();
        $user=$User->get_find(['id'=>session('home_user')['id']],'balance_level,is_balance');//获取当前用户信息
        $balance_set=$this->find_balance();
        if($user['is_balance']==1){
            $user['balance_rate']=$balance_set['balance_rate_'.$user['balance_level']];
        }else{
            $user['balance_rate']=1;
        }
        $user['balance_level']=$this->get_balance_level($user['balance_level']);
        return $user;
    }

    /**
     * 千分位转化
     * @param $data
     * @return string
     */
    public function change_number_to_thousands($data){

        return number_format($data,2,'.',',');
    }

    /**
     * 获取股东的最低兑出金额设置
     * @param $user
     */
    public function get_gudong_min_mix_out_set($user){
        if($user['group_id']==2){
            return $user;
        }
        $father=User::where('id','=',$user['pid'])->field('id,pid,group_id,balance_level,min_mix_out,max_mix_out')->find();
        return $this->get_gudong_min_mix_out_set($father);
    }
}