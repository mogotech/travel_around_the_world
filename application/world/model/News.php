<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use app\world\model\NewsUser;

class News extends Model
{
    /**
     * 设置软删除
     * @author 
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_news';
    /**
     * 自动写入时间戳
     * @author 
     */
    protected $autoWriteTimestamp = true;


    /**
     * 添加消息
     * @param $data  資產信息
     * @return $this
     */
    public function add_news($data)
    {
        $b = self::create($data);
        if ($b) {
            $data['news_id'] = $b->id;
            $NewsUser = new NewsUser();
            $bs = $NewsUser->add_news_user($data);
            if (!$bs) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }


    /**
     * 查询所有消息并分页
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_select($map = '', $order = '', $p = '')
    {
        return self::where($map)->order($order)->paginate($p);
    }


    /**
     * 查询一条消息
     * @param string $map 查询条件
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_find($map = '')
    {
        return self::where($map)->find();
    }

    /**
     * 编辑消息
     * @param string $map 修改信息
     * @return array|false|\PDOStatement|string|Model
     */
    public function edit($map)
    {
        self::update($map);
        $NewsUser = new NewsUser();
        $NewsUser->edit($map);
        return true;
    }



    /**
     * 删除消息
     * @param string $id 要删除的消息id
     * @return array|false|\PDOStatement|string|Model
     */
    public function deletes($id)
    {
        self::destroy(['id' => $id]);
        $NewsUser = new NewsUser();
        $NewsUser->deletes($id);
        return true;
    }

    /**
     * 获取公告列表
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function get_news(){
        //获取公告
        $id = session('home_user')['id'];
        $User=new User();
        $pid=$User->get_pid($id);
        $map=[
            'pid'=>['in',[0,$pid]]
        ];
        $all_news=self::where($map)->limit(10)->select();
        return $all_news;
    }
}