<?php
/**
 * Copyrigh () 2017 ����ħ�����Ƽ����޹�˾ ��Ȩ����
 * ��ַ��http://www.mogo.club
 * ��Ŀ���ƣ��������罻��ƽ̨
 * �ļ����ƣ�Finance.php
 * ʱ�䣺2017��8��25��
 * ���ߣ�
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;

class WalletRuls extends Model
{
    /**
     * 设置软删除
     * @author 
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_wallet_ruls';
    /**
     * 自动写入时间戳
     * @author 
     */
    protected $autoWriteTimestamp = true;

    /**
     * 添加数据
     */
    public function add($data)
    {
        return self::create($data);
    }

    /**
     * 查询数据并分页
     */
    public function get_all($map, $order = '', $p = 15)
    {
        return self::where($map)->order($order)->paginate($p);
    }

    /**
     * 查询数据并分页
     */
    public function get_vive($map, $order = '', $p = 15)
    {
        return self::view('world_wallet_ruls', true)
            ->view('world_user', ['username', 'nickname'], 'world_user.id=world_wallet_ruls.user_id')
            ->where($map)
            ->order($order)
            ->paginate($p);
    }

}