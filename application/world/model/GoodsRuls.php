<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：GoodsRuls.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;

class GoodsRuls extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_goods_ruls';
    /**
     * 自动写入时间戳
     * @author
     */
    protected $autoWriteTimestamp = true;

    /**
     * 自动完成-时间格式改为时间戳
     * @param $value  时间格式
     * @return false|int
     */
    protected function setTimeAttr($value)
    {
        return strtotime($value);
    }


    /**
     * 新增資產价格节点
     * @param $data  价格节点信息 (必须含有goods_id)
     * @return $this
     */
    public function add_goods_ruls($data)
    {
        return self::create($data);
    }


    /**
     * 编辑資產价格节点
     * @param $data 价格节点信息 (必须含有goods_id)
     * @return $this
     */
    public function edit_goods_ruls($data)
    {
        return self::update($data);
    }


    /**
     * 查询一个資產下所有价格节点
     * @param string $map 查询条件(必须含有goods_id)
     * @param string $order 排序字段
     * @param string $p 分页显示条数
     * @return \think\Paginator
     */
    public function get_select($map = '', $order = '', $p = '')
    {
        return self::where($map)->order($order)->paginate($p);
    }


    /**
     * 查询一个資產价格节点
     * @param string $map 查询条件
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_find($map = '')
    {
        return self::where($map)->find();
    }


    /**
     * 软删除一个数组
     * @param $id
     * @return int
     */
    public function del($id)
    {
        return self::destroy($id);
    }


}