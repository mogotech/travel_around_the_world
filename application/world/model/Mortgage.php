<?php
/**
 * Copyrigh () 2017 ����ħ�����Ƽ����޹�˾ ��Ȩ����
 * ��ַ��http://www.mogo.club
 * ��Ŀ���ƣ��������罻��ƽ̨
 * �ļ����ƣ�Finance.php
 * ʱ�䣺2017��8��25��
 * ���ߣ�
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;

class Mortgage extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_mortgage';
    /**
     * 自动写入时间戳
     * @author
     */
    protected $autoWriteTimestamp = true;

    /**
     * 添加信息
     * @param $data  信息
     * @return $this
     */
    public function add($data)
    {
        return self::create($data);
    }

    /**
     * 查询所有数据并分页
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_all($map, $order = '', $p = 15)
    {
        return self::view('world_mortgage mortgage', true)
            ->view('world_order order', ['id' => 'db_order_id', 'surplus_sum', 'pid' => 'order_pid'], 'order.id=mortgage.order_id')
            ->view('world_goods goods', ['id' => 'goods_id', 'name' => 'goods_name'], 'order.goods_id=goods.id')
            ->view('world_user mortgage_user', ['username' => 'mortgage_username', 'nickname' => 'mortgage_nickname', 'money1' => 'mortgage_money1'], 'mortgage_user.id=mortgage.mortgage_id')
            ->view('world_user user', ['username', 'nickname', 'money1'], 'user.id=mortgage.user_id')
            ->where($map)
            ->order($order)
            ->paginate($p);
    }

    /**
     * 计算手续费
     * @param $id  id
     * @$info
     */
    public function mortgage_ruls($id)
    {
        $data = self::get($id);
        if ($data->time) {
            if ($data->time_end) {
                $info['past_time'] = floor(($data->time_end - $data->time) / 86400);
                $info['redeem_money'] = ($info['past_time'] + 1) * $data->mortgage * $data->money;
                $info['redeem_money'] = $this->sctonum($info['redeem_money']);
            } else {
                $info['past_time'] = floor((time() - $data->time) / 86400);
                $info['redeem_money'] = ($info['past_time'] + 1) * $data->mortgage * $data->money;
                $info['redeem_money'] = $this->sctonum($info['redeem_money']);
            }
        } else {
            $info['past_time'] = 0;         //天数
            $info['redeem_money'] = 0;      //金额
        }

        return $info;
    }


    /**
     * 计算单个用户累计收益
     * @param $id  id
     * @$info
     */
    public function get_mortgage_profit($id)
    {
        $money = 0;
        $list = self::where(['mortgage_id' => $id])->select();
        foreach ($list as $value) {
            if ($value->status == 2) {
                $past_time = floor(($value->time_end - $value->time) / 86400);
                $money += $past_time + 1 * $value->mortgage * $value->money;
            }
            if ($value->status == 3) {
                $money += $value->goods_money * $value->sum;
            }
        }
        return $money;
    }


    /**
     * 把科学记数法转换成数字
     * @param $num
     * @param int $double
     * @return string
     */
    function sctonum($num, $double = 5)
    {
        if (false !== stripos($num, "e")) {
            $a = explode("e", strtolower($num));
            return bcmul($a[0], bcpow(10, $a[1], $double), $double);
        }
        return $num;
    }


}