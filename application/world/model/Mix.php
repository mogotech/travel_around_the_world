<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use app\world\model\NewsUser;

class Mix extends Model
{
    /**
     * 设置软删除
     * @author 
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_mix';
    /**
     * 自动写入时间戳
     * @author 
     */
    protected $autoWriteTimestamp = true;

    /**
     * 关联用户
     * @author 
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * 添加信息
     * @param $data  信息
     * @return $this
     */
    public function add_mix($data)
    {
        return self::create($data);
    }


    /**
     * 查询所有数据并分页
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_select($map = '', $order = '', $p = '')
    {
        return self::where($map)->order($order)->paginate($p);
    }


    /**
     * 视图查询关联用户表
     * @param $map
     * @param string $p
     * @return \think\Paginator
     */
    public function get_list($map, $order, $p = '')
    {
        return self::view('world_mix', true)
            ->view('world_user', 'username,nickname', 'world_user.id=world_mix.user_id', 'LEFT')
            ->where($map)
            ->order($order)
            ->paginate($p);
    }


    /**
     * 查询一条数据-关联用户表
     * @param string $map 查询条件
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_find($map = '')
    {
        return self::view('world_mix', true)
            ->view('world_user', 'username,nickname', 'world_user.id=world_mix.user_id', 'LEFT')
            ->where($map)
            ->find();
    }


}