<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use app\world\model\Mix;
use app\world\model\MixOut;
use app\world\model\Order;
use app\world\model\Trans;
use app\world\model\Wallet;
use app\world\model\Mortgage;
use think\Model;

class Test extends Model
{

    static public function index($id)
    {
        $money = 0; //余额
        $money1 = 0;//电子钱包
        //佣金
        $money = Commission::where('pid', '=', $id)->sum('money');
        $data['money'] = round($money, 2);
        $data['money1'] = round($money1, 2);

        //兑入
        $money += Mix::where(['user_id' => $id, 'status' => 2])->sum('mix_money');
        $money += Mix::where(['user_id' => $id, 'status' => 4])->sum('mix_money');
        //兑出
        $money -= MixOut::where(['user_id' => $id, 'status' => 1])->sum('mix_money');
        //买入审核中的资产
        $order1 = Order::where(['user_id' => $id, 'type' => 1, 'status' => 0])->select();
        foreach ($order1 as $value) {
            $a = $value->sum * $value->money;       //资产价值
            $b = $a * $value->purchase_fee;       //手续费
            $money = $money - $a - $b;
        }

        //买入成功的资产
        $order2 = Order::where(['user_id' => $id, 'type' => 1, 'status' => 1])->select();
        foreach ($order2 as $val) {
            $a = $val->actual_sum * $val->money;    //资产价值
            $b = $a * $val->purchase_fee;            //手续费
            $money = $money - $a - $b;
        }


        //卖出成功的资产
        $order3 = Order::where(['user_id' => $id, 'type' => 2, 'status' => 1])->select();
        foreach ($order3 as $v) {
            $a = $v->sum * $v->money2;    //资产价值
            $b = $a * $v->sell_fee;            //手续费
            $money = $money + $a - $b;
        }


        //定向转入
        $moneya = Trans::where(['receive_id' => $id])->sum('money');

        $money += $moneya;

        //定向转出
        $moneyb = Trans::where(['user_id' => $id])->sum('money');

        $money -= $moneyb;


        //转电子
        $money1 = Wallet::where(['user_id' => $id, 'type' => 1])->sum('money');
        $money -= $money1;

        //电子钱包转余额
        $money2 = Wallet::where(['user_id' => $id, 'type' => 2])->sum('money');
        $money += $money2;
        $money1 -= $money2;


        //抵押资产加钱
        $money += Mortgage::where(['user_id' => $id, 'status' => 1])->sum('money');

        //抵押资产减钱
        $money -= Mortgage::where(['mortgage_id' => $id, 'status' => 1])->sum('money');


        //抵押资产赎回减钱
        $mortgage1 = Mortgage::where(['user_id' => $id, 'status' => 2])->select();
        foreach ($mortgage1 as $vo) {
            $time = floor(($vo->time_end - $vo->time) / 86400) + 1;
            $lixi = $time * $vo->money * $vo->mortgage;
            $money -= $lixi;
        }


        //抵押资产赎回加钱
        $mortgage1 = Mortgage::where(['mortgage_id' => $id, 'status' => 2])->select();
        foreach ($mortgage1 as $vo) {
            $time = floor(($vo->time_end - $vo->time) / 86400) + 1;
            $lixi = $time * $vo->money * $vo->mortgage;
            $money += $lixi;
        }

        //买入审核中的配资资产
        $order1 = Order::where(['user_id' => $id, 'type' => 4, 'status' => 0])->select();
        foreach ($order1 as $value) {
            $a = $value->sum * $value->money;       //资产价值
            $b = $a * $value->purchase_fee;       //手续费
            $user_money = round($a / ($value->capital_pro + 1), 2); //融资人支付金额
            $money = $money - $user_money - $b;
        }


        //买入的配资资产
        $order1 = Order::where(['user_id' => $id, 'type' => 4, 'status' => 1])->select();
        foreach ($order1 as $value) {
            $a = $value->actual_sum * $value->money;       //资产价值
            $b = $a * $value->purchase_fee;       //手续费
            $user_money = round($a / ($value->capital_pro + 1), 2); //融资人支付金额
            $money = $money - $user_money - $b;
        }


        //配资商同意配资资产
        $order1 = Order::where(['transfer_id' => $id, 'type' => 4, 'status' => 1])->select();
        foreach ($order1 as $value) {
            $a = $value->actual_sum * $value->money;       //资产价值
            $financing_money = round($a / ($value->capital_pro + 1) * $value->capital_pro, 2); //配资商支付金额
            $money = $money - $financing_money;
        }


        //卖出配资资产-融资人
        $order1 = Order::where(['user_id' => $id, 'type' => 5, 'status' => 1])->select();
        foreach ($order1 as $value) {
            $money += $value->user_money;
        }


        //卖出配资资产-配资商
        $order1 = Order::where(['transfer_id' => $id, 'type' => 5, 'status' => 1])->select();
        foreach ($order1 as $value) {
            $money += $value->financing_money;
        }


        $data['money'] = round($money, 2);
        $data['money1'] = round($money1, 2);
        return $data;
    }

}