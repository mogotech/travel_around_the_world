<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use app\world\model\User;

class NewsUser extends Model
{
    /**
     * 设置软删除
     * @author 
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_news_user';
    /**
     * 自动写入时间戳
     * @author 
     */
    protected $autoWriteTimestamp = true;


    /**
     * 后台添加用户消息
     * @param $data  資產信息
     * @return $this
     */
    public function add_news_user($data)
    {
        $UserModel = new User();
        $users = $UserModel->get_select();
        $users = collection($users)->toArray();
        if ($data['pid'] == 0) {
            $info = $users;
        } else {
            $info = get_children_id($users, $data['pid']);
            array_push($info, ['id' => $data['pid']]);
        }

        foreach ($info as $value) {
            $data['user_id'] = $value['id'];
            $b = self::create($data);
            if (!$b) {
                return false;
            }
        }
        return true;
    }


    /**
     * 查询用户所有消息并分页
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_select($map = '', $order = '', $p = '')
    {
        return self::where($map)->order($order)->paginate($p);
    }

    /**
     * 查询一条消息
     * @param $map
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_find($map)
    {
        return self::where($map)->find();
    }

    /**
     * 修改用户消息
     * @param $data  需要修改的用户资料(必须含有用户id)
     * @return $this
     */
    public function edit_news_user($data)
    {
        return self::update($data);
    }


    /**
     * 后台修改消息
     * @param $data  需要修改的父级消息
     * @return $this
     */
    public function edit($data)
    {
        self::save([
            'type' => $data['type'],
            'link' => $data['link'],
            'title' => $data['title'],
            'content' => $data['content'],
        ], ['news_id' => $data['id']]);
    }


    /**
     * 后台删除消息
     * @param $data  需要删除的父级消息
     * @return $this
     */
    public function deletes($id)
    {
        self::destroy(['news_id' => $id]);
    }


}