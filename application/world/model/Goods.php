<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Goods.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use think\Db;
use app\world\model\User;
use app\world\model\Configs;
use app\world\model\GoodsPrice;
use app\world\model\Order;
use app\world\model\NewsUser;

class Goods extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_goods';
    /**
     * 自动写入时间戳
     * @author
     */
    protected $autoWriteTimestamp = true;


    /**
     * 关联用户
     * @author
     */
    public function user()
    {
        return $this->belongsTo('User')->field('id,nickname');
    }


    /**
     * 关联資產历史价格
     * @author
     */
    public function goodsprice()
    {
        return $this->hasMany('GoodsPrice');
    }


    /**
     * 关联订单
     * @author
     */
    public function order()
    {
        return $this->hasMany('Order');
    }

    /**
     * 关联价格节点
     * @author
     */
    public function goodsruls()
    {
        return $this->hasMany('GoodsRuls');
    }


    /**
     * 新增資產
     * @param $data  資產信息
     * @return $this
     */
    public function add_goods($data)
    {
        return self::create($data);
    }


    /**
     * 编辑資產
     * @param $data  資產信息
     * @return $this
     */
    public function edit_goods($data)
    {
        return self::update($data);
    }


    /**
     * 查询所有資產并分页
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_select($map = '', $order = '', $p = '')
    {
        return self::view('world_goods', true)
            ->view('world_user', 'username,nickname', 'world_user.id=world_goods.user_id', 'LEFT')
            ->where($map)
            ->order($order)
            ->paginate($p);
    }

    /**
     * @param string $map 查询条件
     * @param string $order 排序方式
     * @param string $field 查询字段
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function get_some_goods($map = '', $order = '', $field = '*')
    {
        return self::where($map)->order($order)->field($field)->select();
    }

    /**
     * 查询一个資產
     * @param string $map 查询条件
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_find($map = '')
    {
        return self::where($map)->find();
    }


//    /**
//     * 查询一个資產详情-关联用户表和資產历史价格表
//     * @param $goods_id  資產id
//     * @param $time   根据历史价格时间进行查询
//     * @return array|false|\PDOStatement|string|\think\Collection
//     */
//    public function get_main($goods_id, $time = 'day')
//    {
//        $model = self::get($goods_id);
//        if ($time == 'day') {
//            $date_time = mktime(0, 0, 0, date('m'), date('d'), date('y'));      //今天开始
//
//        }
//        if ($time == 'zhou') {
//            $date_time = strtotime(date('Y-m-d 00:00:00', strtotime('-7 days')));  //七天前
//        }
//
//        if ($time == 'yue') {
//            $date_time = strtotime(date('Y-m-d 00:00:00', strtotime('-30 days')));
//        }
//
//        if ($time == 'nian') {
//            $date_time = strtotime(date('Y-m-d 00:00:00', strtotime('-365 days')));
//        }
//
//        if ($time == 'all') {
//            $date_time = strtotime('2010-00-00 00:00:00');
//        }
//
//        $where = [$date_time, mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1];
//
//        return $model->goodsprice()->where('create_time', 'between time', $where)->select();
//    }


    /**
     * 查询一个資產详情-关联用户表和資產历史价格表
     * @param $goods_id  資產id
     * @return array|false|\PDOStatement|string|\think\Collection
     */
    public function get_main1($goods_id)
    {
        $data = Db::name('world_goods_price')->where(['goods_id' => $goods_id])->order('create_time')->select();
        $i = 0;
        foreach ($data as $key => $value) {
            $info[] = $value;
            $money[$key] = $value['money'];
            $time[$key] = $value['create_time'];
            if (count($info) == 4) {
                //最低价格
                $b = my_sort($info, 'money', SORT_ASC);
                $list[$i]['minimum_money'] = $b[0]['money'];
                //最高价格
                $b = my_sort($info, 'money', SORT_DESC);
                $list[$i]['highest_money'] = $b[0]['money'];
                //开盘价格
                $b = my_sort($info, 'create_time', SORT_ASC);
                $list[$i]['start_money'] = $b[0]['money'];
                //封盘价格
                $b = my_sort($info, 'create_time', SORT_DESC);
                $list[$i]['end_money'] = $b[0]['money'];
                //封盘时间
                $list[$i]['end_time'] = $b[0]['create_time'];
                $i++;
                $money = array();
                $time = array();
                $info = array();
            }

        }
        return @$list;
    }


//    /**
//     * 查询一个資產详情-关联用户表和資產历史价格表
//     * @param $goods_id  資產id
//     * @return array|false|\PDOStatement|string|\think\Collection
//     */
//    public function get_main($goods_id)
//    {
//        $goods = Db::name('world_goods')->where(['id' => $goods_id])->find();
//        if ($goods['user_id']) {
//            return $this->get_main1($goods_id);
//        }
//        $data = Db::name('world_goods_price')->where(['goods_id' => $goods_id])->order('create_time')->select();
//        $bs = '';
//        $i = 0;
//        foreach ($data as $key => $value) {
//            $time = date('y-m', $value['create_time']);
//            if ($time == $bs) {
//                $info[] = $value;
//            } else {
//                if (@$info) {
//                    //最低价格
//                    $b = my_sort($info, 'money', SORT_ASC);
//                    $list[$i]['minimum_money'] = $b[0]['money'];
//                    //最高价格
//                    $b = my_sort($info, 'money', SORT_DESC);
//                    $list[$i]['highest_money'] = $b[0]['money'];
//                    //开盘价格
//                    $b = my_sort($info, 'create_time', SORT_ASC);
//                    $list[$i]['start_money'] = $b[0]['money'];
//                    //封盘价格
//                    $b = my_sort($info, 'create_time', SORT_DESC);
//                    $list[$i]['end_money'] = $b[0]['money'];
//                    //封盘时间
//                    $list[$i]['end_time'] = $b[0]['create_time'];
//                    $info = array();
//                    $i++;
//                }
//                $bs = $time;
//                $info[] = $value;
//
//            }
//        }
//        $b = my_sort($info, 'money', SORT_ASC);
//        $list[$i]['minimum_money'] = $b[0]['money'];
//        //最高价格
//        $b = my_sort($info, 'money', SORT_DESC);
//        $list[$i]['highest_money'] = $b[0]['money'];
//        //开盘价格
//        $b = my_sort($info, 'create_time', SORT_ASC);
//        $list[$i]['start_money'] = $b[0]['money'];
//        //封盘价格
//        $b = my_sort($info, 'create_time', SORT_DESC);
//        $list[$i]['end_money'] = $b[0]['money'];
//        //封盘时间
//        $list[$i]['end_time'] = $b[0]['create_time'];
//
//        return $list;
//    }


    /**
     * 查询一个資產详情-关联用户表和資產历史价格表
     * @param $goods_id  資產id
     * @return array|false|\PDOStatement|string|\think\Collection
     */
    public function get_main($goods_id)
    {
        $goods = Db::name('world_goods')->where(['id' => $goods_id])->find();
        if ($goods['user_id']) {
            return $this->get_main1($goods_id);
        }
        $data = Db::name('world_goods_price')->where(['goods_id' => $goods_id, 'create_time' => ['LT', '1512576000']])->order('create_time')->select();
        $bs = '';
        $i = 0;
        foreach ($data as $key => $value) {
            $time = date('y-m', $value['create_time']);
            if ($time == $bs) {
                $info[] = $value;
            } else {
                if (@$info) {
                    //最低价格
                    $b = my_sort($info, 'money', SORT_ASC);
                    $list[$i]['minimum_money'] = $b[0]['money'];
                    //最高价格
                    $b = my_sort($info, 'money', SORT_DESC);
                    $list[$i]['highest_money'] = $b[0]['money'];
                    //开盘价格
                    $b = my_sort($info, 'create_time', SORT_ASC);
                    $list[$i]['start_money'] = $b[0]['money'];
                    //封盘价格
                    $b = my_sort($info, 'create_time', SORT_DESC);
                    $list[$i]['end_money'] = $b[0]['money'];
                    //封盘时间
                    $list[$i]['end_time'] = $b[0]['create_time'];
                    $info = array();
                    $i++;
                }
                $bs = $time;
                $info[] = $value;

            }
        }

        $b = my_sort($info, 'money', SORT_ASC);
        $list[$i]['minimum_money'] = $b[0]['money'];
        //最高价格
        $b = my_sort($info, 'money', SORT_DESC);
        $list[$i]['highest_money'] = $b[0]['money'];
        //开盘价格
        $b = my_sort($info, 'create_time', SORT_ASC);
        $list[$i]['start_money'] = $b[0]['money'];
        //封盘价格
        $b = my_sort($info, 'create_time', SORT_DESC);
        $list[$i]['end_money'] = $b[0]['money'];
        //封盘时间
        $list[$i]['end_time'] = $b[0]['create_time'];


        $data = Db::name('world_goods_price')->where(['goods_id' => $goods_id, 'create_time' => ['EGT', '1512576000']])->order('create_time')->select();
        foreach ($data as $key => $value) {
            $time = date('y-m-d', $value['create_time']);
            if ($time == $bs) {
                $info[] = $value;
            } else {
                if (@$info) {
                    //最低价格
                    $b = my_sort($info, 'money', SORT_ASC);
                    $list[$i]['minimum_money'] = $b[0]['money'];
                    //最高价格
                    $b = my_sort($info, 'money', SORT_DESC);
                    $list[$i]['highest_money'] = $b[0]['money'];
                    //开盘价格
                    $b = my_sort($info, 'create_time', SORT_ASC);
                    $list[$i]['start_money'] = $b[0]['money'];
                    //封盘价格
                    $b = my_sort($info, 'create_time', SORT_DESC);
                    $list[$i]['end_money'] = $b[0]['money'];
                    //封盘时间
                    $list[$i]['end_time'] = $b[0]['create_time'];
                    $info = array();
                    $i++;
                }
                $bs = $time;
                $info[] = $value;

            }

        }

        $b = my_sort($info, 'money', SORT_ASC);
        $list[$i]['minimum_money'] = $b[0]['money'];
        //最高价格
        $b = my_sort($info, 'money', SORT_DESC);
        $list[$i]['highest_money'] = $b[0]['money'];
        //开盘价格
        $b = my_sort($info, 'create_time', SORT_ASC);
        $list[$i]['start_money'] = $b[0]['money'];
        //封盘价格
        $b = my_sort($info, 'create_time', SORT_DESC);
        $list[$i]['end_money'] = $b[0]['money'];
        //封盘时间
        $list[$i]['end_time'] = $b[0]['create_time'];


        return $list;
    }


    /**
     * 关联查询資產价格节点
     * @param $map
     * @param string $order
     * @param int $p
     * @return \think\Paginator
     */
    public function get_goods_ruls($map, $order = '')
    {
        $data = self::where($map)->find();
        return $data->goodsruls()->order($order)->select();
    }


    /**
     * 刷新資產价格-查询需要更新的資產
     * @author
     */
    public function update_money()
    {

        $Configs = new Configs();
        $con = $Configs->get_find();
        $start_time = $con->start_time;   //开市时间
        $end_time = $con->end_time;       //闭市时间
        if (time() > strtotime($start_time) && time() < strtotime($end_time)) {
            $sql = "select * from dp_world_goods where  delete_time is NULL";
            //$sql = "select * from dp_world_goods where time+3600 <= UNIX_TIMESTAMP() and delete_time is NULL";
            $data = Db::query($sql); //需要更新的資產
            if ($data) $this->is_goods_ruls($data);
            //添加自动平仓
            $Order = new Order();
            $Order->automatic_warehouse();
        }

    }


    /**
     * 資產是否有设置价格节点
     * @param $data 需要更新的資產信息
     */
    protected function is_goods_ruls($data)
    {
        foreach ($data as $v) {
            $ruls_sql = "select * from dp_world_goods_ruls 
                                where 
                                    goods_id = " . $v['id'] . " and  times > UNIX_TIMESTAMP()
                                order 
                                    BY times
                                limit 1";
            $ruls_data = Db::query($ruls_sql);
            if ($ruls_data) {  //有设置价格节点
                $this->get_NewMoney($v, $ruls_data[0]);
            } else {  //没有有设置价格节点
                $this->get_NewMoney($v, '');
            }

        }

    }


    /**
     * 计算資產当前标准价格
     * @param $data   需要更新的資產信息
     * @param string $ruls_data 价格节点信息
     */
    protected function get_NewMoney($data, $ruls_data = '')
    {
        //查询上一次的价格节点
        $ruls_sql = "select * from dp_world_goods_ruls
                                where
                                    goods_id = " . $data['id'] . " and  times <= UNIX_TIMESTAMP()
                                order
                                    BY times desc
                                limit 1";
        $info = Db::query($ruls_sql);
        if (time() - $info[0]['times'] < 120) {//当前时间大于最后一次价格节点 少于2分钟
            $money = $info[0]['money'];  //价格节点
            $benchmark_money = $info[0]['money'];  //价格标准
        } else {
            if ($ruls_data) {   //有设置价格节点
                $a_time = $ruls_data['times'] - $data['time'];  //设置的节点时间  减去 商品最后一次更新的时间
                $a_money = $ruls_data['money'] - $data['benchmark_money'];//设置的节点价格  减去 商品的价格基准
                $a = $a_money / $a_time;
                $b = $data['benchmark_money'] - $a * $data['time'];
                $benchmark_money = time() * $a + $b;     //新的价格标准
            } else {
                $benchmark_money = $data['benchmark_money']; //价格标准
            }
            $money = get_rand($benchmark_money - $data['section'], $benchmark_money + $data['section']); //当前价格
            //$money = mt_rand($benchmark_money - $data['section'], $benchmark_money + $data['section']);
            //$f = mt_rand(1, 100); //随机小数
            //$money = $money + ($f / 100);
            $money <= 0 ? $money = 0.01 : ''; //价格最低为0
        }

        $this->save_goods_money($data['id'], $money, $benchmark_money);
    }


    /**
     * 更新資產价格并存入数据库
     * @param $id  要更新的資產价格id
     * @param $money    当前实际金额
     */
    protected function save_goods_money($id, $money, $benchmark_money)
    {
        //更新資產价格
        $update_sql = "update dp_world_goods 
                        set  
                           money = $money ,
                           time = UNIX_TIMESTAMP(),
                             benchmark_money=$benchmark_money
                        where 
                          id = " . $id;
        Db::query($update_sql);
        //添加历史資產价格记录
        $GoodsPrice = new GoodsPrice;
        $GoodsPrice->goods_id = $id;
        $GoodsPrice->money = $money;
        $GoodsPrice->save();
    }


    /**
     * 得到商品历史参数
     * @param $id 商品id
     * @param
     */
    public function get_history($id)
    {
        $data = Db::name('world_goods_price')->where(['goods_id' => $id])->select();
        $sum = 0;
        foreach ($data as $key => $value) {
            $money[$key] = $value['money'];
            $sum += $value['money'];
        }
        $history = array();
        if ($data) {
            array_multisort($money, SORT_DESC, $data);
            $history['highest'] = $data[0]['money'];
            array_multisort($money, SORT_ASC, $data);
            foreach ($data as $v){
                if($v['money']>0){
                    $history['minimum'] = $v['money'];

                    break;
                }
            }
            $history['average'] = round($sum / count($data), 2);
            $zuo_start = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
            $zuo_end = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
            $wheres = [$zuo_start, $zuo_end];
            $where['create_time'] = ['between', $wheres];
            $where['goods_id'] = $id;
            $zuo_money = Db::name('world_goods_price')->where($where)->order('create_time desc')->find();
            $qian_start = mktime(0, 0, 0, date('m'), date('d') - 2, date('Y'));
            $qian_end = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')) - 1;
            $maps = [$qian_start, $qian_end];
            $map['create_time'] = ['between', $maps];
            $map['goods_id'] = $id;
            $qian_money = Db::name('world_goods_price')->where($map)->order('create_time desc')->find();

            if (isset($zuo_money['money']) && isset($qian_money['money']) && $qian_money['money'] != 0) {
                $a = round((($zuo_money['money'] - $qian_money['money']) / $qian_money['money'] * 100), 2);
                $a > 0 ? $history['proportion'] = '+' . $a . '%' : $history['proportion'] = $a . '%';
            }
        }

        return $history;
    }

    /**
     *查询所有审核的订单
     */
    public function get_goods()
    {
        $data = self::where(['is_examine' => 1])->select();
        return $data;
    }

    /**
     * 平仓
     */
    public function apitalc()
    {
        set_time_limit(0);
        $pageshow = 500;

        $second = 1;

        $count = Db::view('world_order')
            ->view('world_goods', ['name' => 'goods_name', 'money' => 'goods_money'], 'world_goods.id=world_order.goods_id')
            ->where(['world_order.type' => 4, 'world_order.status' => 1, 'world_order.surplus_sum' => ['>', 0], 'world_order.is_capital' => 1])
            ->count();

        for ($i = 0; $i < ceil($count / $pageshow); $i++) {
            $pagesize = $i * $pageshow;
            $order_data = Db::view('world_order')
                ->view('world_goods', ['name' => 'goods_name', 'money' => 'goods_money'], 'world_goods.id=world_order.goods_id')
                ->where(['world_order.type' => 4, 'world_order.status' => 1, 'world_order.surplus_sum' => ['>', 0], 'world_order.is_capital' => 1])
                ->limit($pagesize, $pageshow)
                ->select();

            foreach ($order_data as $value) {

                //当前资产价值
                $money = round($value['goods_money'] * $value['surplus_sum'] + $value['money3'], 2);

                if ($money >= $value['margin_money']) {//大于补仓线
                    Order::update([
                        'bond_prompt' => 0
                    ], ['id' => $value['id']]);
                }


                if ($money < $value['margin_money'] && $money > $value['selling_money']) {  //小于补仓线
                    NewsUser::create([
                        'title' => '資產配資。Asset financing',
                        'content' => '您的配資資產小於補倉綫，請及時繳納保證金。Your assets with less than margin line, please pay the deposit.',
                        'user_id' => $value['user_id'],
                    ]);

                    Order::update([
                        'bond_prompt' => 1
                    ], ['id' => $value['id']]);
                }

                if ($money <= $value['selling_money']) {  //小于平仓线
                    NewsUser::create([
                        'title' => '資產配資。Asset financing',
                        'content' => '您的配資資產小於平倉綫，請及時繳納保證金。If your assets are less than the flat line, please pay the deposit in time.',
                        'user_id' => $value['user_id'],
                    ]);

                    NewsUser::create([
                        'title' => '資產配資。Asset financing',
                        'content' => '您的配資資產小於平倉綫，請及時平倉避免損失。Your assets are less than the flat line, please keep it in time to avoid the loss.',
                        'user_id' => $value['transfer_id'],
                    ]);


                    if ($value['is_automatic']) {      //自动平仓
                        Order::create([
                            'order_id' => $value['id'],
                            'type' => 5,
                            'pid' => $value['pid'],
                            'transfer_id' => $value['transfer_id'],
                            'user_id' => $value['user_id'],
                            'goods_id' => $value['goods_id'],
                            'money' => $value['money'],
                            'money2' => $value['goods_money'],
                            'money3' => $value['money3'],
                            'sum' => $value['surplus_sum'],
                            'actual_sum' => $value['surplus_sum'],
                            'surplus_sum' => $value['surplus_sum'],
                            'status' => 0,
                            'purchase_fee' => $value['purchase_fee'],
                            'is_capital' => 1,
                            'margin' => $value['margin'],
                            'selling_money' => $value['selling_money'],
                            'margin_money' => $value['margin_money'],
                            'capital_pro' => $value['capital_pro'],
                            'is_automatic' => 1,
                            'time' => $value['time'],
                            'end_time' => $value['end_time'],
                            'capital_interest' => $value['capital_interest'],
                            'is_selling' => 1,
                            'is_force' => 1,
                            'margin_price' => $value['margin_price'],
                            'selling_price' => $value['selling_price'],
                        ]);

                        Order::update([
                            'surplus_sum' => 0,
                            'is_selling' => 1,
                            'is_force' => 1,
                        ], ['id' => $value['id']]);


                        NewsUser::create([
                            'title' => '資產配資。Asset financing',
                            'content' => '您的配資資產已自動平倉，請查看配資資產。Your assets have been automatically closed, please check the assets.',
                            'user_id' => $value['user_id'],
                        ]);

                        NewsUser::create([
                            'title' => '資產配資。Asset financing',
                            'content' => '您的配資資產已自動平倉。Your assets have been automatically closed.',
                            'user_id' => $value['transfer_id'],
                        ]);
                    }
                }
            }
            sleep($second);
        }
    }

    public function get_shareholder_data($id = '')
    {
        $Order = new Order();
        $map = array(
            'pid' => $id,
            'type' => 1,
            'surplus_sum' => ['>', 0],
        );
        $order_datas = $Order::where($map)->field('money,surplus_sum,goods_id,buy_type,user_id')->select();//这个
//            echo json_encode($order_datas);die;
        $order_sum_data=$Order::where($map)->field('*')->group('user_id')->select();
//        $order_datas_ceshi= $Order::where($map)->where('goods_id','31')->where('buy_type','neq','2')->field('money,surplus_sum,goods_id,buy_type,user_id')->group('user_id')->select();//这个
//        echo json_encode($order_datas_ceshi);die;
        $map1 = array(
            'pid' => $id,
            'type' => ['in', [2, 5]],
            'surplus_sum' => ['>', 0],
            'status' => ['in',[0,1]]
        );
        $sell_orders = $Order::where($map1)->field('money,surplus_sum,goods_id,money2,buy_type,status')->select();

        $order_datas1 = array();
        $user_haxi  = [];
        $zk_user_haxi = [];
        foreach ($order_datas as $key => $val) {
            if(!empty($val['buy_type'])&&$val['buy_type']==2){
                $good=Goods::get($val['goods_id']);
                @$order_datas1[$val['goods_id']]['zk_total_money'] += $val['money'] * $val['surplus_sum'];//当前持有总成本
                @$order_datas1[$val['goods_id']]['zk_total_sum'] += $val['surplus_sum'];//当前持有总数量
                @$order_datas1[$val['goods_id']]['zk_total_user_num'];
                if(@!$user_haxi[$val['goods_id']][$val['user_id']]){
                    $user_haxi[$val['goods_id']][$val['user_id']] =1;
                    @$order_datas1[$val['goods_id']]['zk_total_user_num'] +=1;
                }

                @$order_datas1[$val['goods_id']]['zk_total_new_money'] +=(2*$val['money']-$good['money'])* $val['surplus_sum'] ;
            }else{
                @$order_datas1[$val['goods_id']]['total_money'] += $val['money'] * $val['surplus_sum'];//当前持有总成本
                @$order_datas1[$val['goods_id']]['total_sum'] += $val['surplus_sum'];//当前持有总数量
                if(@!$zk_user_haxi[$val['goods_id']][$val['user_id']]){

                    $zk_user_haxi[$val['goods_id']][$val['user_id']] =1;
                    @$order_datas1[$val['goods_id']]['total_user_num'] +=1;
                }
            }
        }

        foreach ($sell_orders as $key => $val){
            if($val['status']==0){
                if(empty($val['buy_type'])&&$val['buy_type']!=2){
                    @$order_datas1[$val['goods_id']]['total_pre_sell_num'] += $val['surplus_sum']; //预卖出数量
                }else{
                    @$order_datas1[$val['goods_id']]['zk_total_pre_sell_num'] += $val['surplus_sum'];//做空预卖出数量
                }

                continue;
            }
            if(empty($val['buy_type'])&&$val['buy_type']!=2) {
                @$order_datas1[$val['goods_id']]['total_profit_loss'] += number_format(($val['money2'] - 2 * $val['money']) * $val['surplus_sum'], 2);

                @$order_datas1[$val['goods_id']]['total_sell_num'] += $val['surplus_sum'];//当前总卖出数量

            }else{
                @$order_datas1[$val['goods_id']]['zk_total_profit_loss'] += number_format(($val['money'] - $val['money2']) * $val['surplus_sum'], 2);//当前总盈亏
                @$order_datas1[$val['goods_id']]['zk_total_sell_num'] += $val['surplus_sum'];//当前总卖出数量
            }
        }
        return $order_datas1;
    }


}