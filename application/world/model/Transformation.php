<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;

class Transformation extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_transformation';
    /**
     * 自动写入时间戳
     * @author
     */
    protected $autoWriteTimestamp = true;


    /**
     * 用户分页输出数据
     * @param $user_id  登陆的用户id
     * @param int $type 1转出输出，2转入数据
     * @param int $p 分页数
     * @return \think\Paginator
     */
    public function get_list($user_id, $order = '', $p = 15)
    {
       return self::where(['user_id' => $user_id])->order($order)->paginate($p);
    }


}