<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;
use traits\model\SoftDelete;
use app\world\model\NewsUser;

class Commission extends Model
{
    /**
     * 设置软删除
     * @author 
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_commission';
    /**
     * 自动写入时间戳
     * @author 
     */
    protected $autoWriteTimestamp = true;

    /**
     * 关联用户
     * @author 
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * 视图查询关联用户表
     * @param $map
     * @param string $map  查询条件
     * @param string $order 排序条件
     * @return \think\Paginator
     */
    public function get_list($map, $order = '')
    {

        return self::view('world_commission', true)
            ->view('world_user', 'username,nickname', 'world_user.id=world_commission.user_id', 'LEFT')
            ->order($order)
            ->where($map)
            ->select();
    }


}