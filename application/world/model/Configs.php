<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Model;


class Configs extends Model
{

    /**
     * 设置数据库表名
     * @author 
     */
    protected $table = 'dp_world_configs';


    /**
     * 查询系统设置
     * @param string $map 查询条件
     * @param string $order 排序
     * @param string $p 每页条数
     * @return \think\Paginator
     */
    public function get_find($field = '')
    {
        return self::field($field)->find(1);
    }


    /**
     * 修改系统配置
     * @param $data  要修改的值
     * @return $this
     */
    public function edit_config($data)
    {
        return self::update($data);
    }

}