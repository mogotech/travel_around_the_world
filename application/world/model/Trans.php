<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\model;

use think\Db;
use think\Model;
use traits\model\SoftDelete;
use app\world\model\User;

class Trans extends Model
{
    /**
     * 设置软删除
     * @author
     */
    use SoftDelete;
    /**
     * 设置数据库表名
     * @author
     */
    protected $table = 'dp_world_trans';
    /**
     * 自动写入时间戳
     * @author
     */
    protected $autoWriteTimestamp = true;


    /**
     * join用户分页输出数据
     * @param $user_id  登陆的用户id
     * @param int $type 1转出输出，2转入数据
     * @param int $p 分页数
     * @return \think\Paginator
     */
    public function get_list($user_id, $type, $order = '', $p = 15)
    {
        if ($type == 1) { //转出
            $data = Db::table('dp_world_trans')
                ->join('dp_world_user', 'dp_world_trans.receive_id=dp_world_user.id')
                ->field('dp_world_user.username,dp_world_user.nickname,dp_world_trans.*')
                ->where(['dp_world_trans.user_id' => $user_id])
                ->order($order)
                ->paginate($p);
        } else if ($type == 2) { //收入
            $data = Db::table('dp_world_trans')
                ->join('dp_world_user', 'dp_world_trans.user_id=dp_world_user.id')
                ->field('dp_world_user.username,dp_world_user.nickname,dp_world_trans.*')
                ->where(['receive_id' => $user_id])
                ->order($order)
                ->paginate($p);
        }
        return @$data;
    }


    /**
     * join用户分页输出数据
     * @param $user_id  登陆的用户id
     * @param int $type 1转出输出，2转入数据
     * @param int $p 分页数
     * @return \think\Paginator
     */
    public function get_subordinate_list($map, $type, $order = '', $p = 15)
    {
        if ($type == 1) { //转出
            $data = Db::table('dp_world_trans')
                ->join('dp_world_user user', 'dp_world_trans.user_id=user.id')//转出
                ->join('dp_world_user receive', 'dp_world_trans.receive_id=receive.id')//接收
                ->field('user.username,user.nickname,dp_world_trans.*,receive.username as r_username,receive.nickname as r_nickname')
                ->where($map)
                ->order($order)

                ->paginate($p);

        } else if ($type == 2) { //收入
            $data = Db::table('dp_world_trans')
                ->join('dp_world_user user', 'dp_world_trans.user_id=user.id')//转出
                ->join('dp_world_user receive', 'dp_world_trans.receive_id=receive.id')//接收
                ->field('user.username,user.nickname,dp_world_trans.*,receive.username as r_username,receive.nickname as r_nickname')
                ->where($map)
                ->order($order)
                ->paginate($p);

        }

        return @$data;
    }


}