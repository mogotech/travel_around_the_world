<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：User.php
 * 时间：2017年8月25日
 * 作者：
 */


namespace app\world\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\world\model\Balance;
use app\world\model\Configs as ConfigsModel;
use think\Request;

/**
 * 用户默认控制器
 * @package app\user\admin
 */
class Configs extends Admin
{


    /**
     * 设置系统
     * @author 
     */
    public function index($id = 1)
    {
        if ($id === null) $this->error('缺少参数');
        $model = new ConfigsModel();
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'Configs');
            if ($validate !== true) $this->error($validate);
            if ($model->edit_config($data)) {
                $this->success('编辑成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }

        $info = $model->get_find();
        return ZBuilder::make('form')
            ->setPageTitle('编辑用户')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['text', 'margin', '补仓线'],
                ['text', 'selling', '平仓线'],
                ['text', 'capproportion1', '配资比例'],
                ['text', 'capproportion2', '配资比例'],
                ['text', 'capproportion3', '配资比例'],
                ['text', 'capproportion4', '配资比例'],
                ['text', 'capproportion5', '配资比例'],
                ['text', 'shop_min', '昨日交易最小值区间'],
                ['text', 'shop_max', '昨日交易最大值区间'],
                ['text', 'overdue', '逾期天数'],
                ['text', 'mortgage_top', '抵押资产价值上限'],
                ['text', 'electronics_wallet', '电子钱包'],
                ['text', 'purchase_fee', '购买资产手续费'],
                ['text', 'sell_fee', '卖出资产手续费'],
                ['text', 'brokers', '同等级经纪商比例'],
                ['text', 'brokers1', '区域经济商'],
                ['text', 'brokers2', '大区经纪商'],
                ['text', 'brokers3', '国际经纪商'],
                ['time', 'start_time', '开市时间'],
                ['time', 'end_time', '闭市时间'],
                ['text', 'exchange', '美金汇率'],
                ['text', 'fee_money', '提现手续费'],
                ['text', 'commission1', '一级佣金'],
                ['text', 'commission2', '二级佣金'],
                ['text', 'commission3', '三级佣金'],
                ['text', 'commission4', '四级佣金'],
                ['text', 'commission5', '五级佣金'],
                ['text', 'commission6', '六级佣金'],
                ['text', 'commission7', '七级佣金'],
                ['text', 'sum_commission1', '一星用戶总佣金'],
                ['text', 'sum_money1', '一星用戶总资产'],
                ['text', 'sum_commission2', '二星用戶总佣金'],
                ['text', 'sum_money2', '二星用戶总资产'],
                ['text', 'sum_commission3', '三星用戶总佣金'],
                ['text', 'sum_money3', '三星用戶总资产'],
                ['text', 'sum_commission4', '四星用戶总佣金'],
                ['text', 'sum_money4', '四星用戶总资产'],
                ['text', 'sum_commission5', '五星用戶总佣金'],
                ['text', 'sum_money5', '五星用戶总资产'],
                ['text', 'sum_commission6', '六星用戶总佣金'],
                ['text', 'sum_money6', '六星用戶总资产'],
                ['text', 'sum_commission7', '七星用戶总佣金'],
                ['text', 'sum_money7', '七星用戶总资产'],
            ])
            ->setFormData($info)// 设置表单数据
            ->layout([
                'margin' => 6, 'selling' => 6,
                'capproportion1' => 6, 'capproportion2' => 6,
                'capproportion3' => 6, 'capproportion4' => 6,
                'mortgage_top' => 6, 'capproportion5' => 6,
                'brokers' => 6, 'brokers1' => 6,
                'shop_min' => 6, 'shop_max' => 6,
                'brokers2' => 6, 'brokers3' => 6,
                'electronics_wallet' => 6, 'overdue' => 6,
                'purchase_fee' => 6, 'sell_fee' => 6,
                'start_time' => 6, 'end_time' => 6,
                'exchange' => 6, 'fee_money' => 6,
                'commission1' => 12, 'commission2' => 6,
                'commission3' => 6, 'commission4' => 6,
                'commission5' => 6, 'commission6' => 6,
                'commission7' => 6,
                'sum_commission1' => 6, 'sum_money1' => 6,
                'sum_commission2' => 6, 'sum_money2' => 6,
                'sum_commission3' => 6, 'sum_money3' => 6,
                'sum_commission4' => 6, 'sum_money4' => 6,
                'sum_commission5' => 6, 'sum_money5' => 6,
                'sum_commission6' => 6, 'sum_money6' => 6,
                'sum_commission7' => 6, 'sum_money7' => 6,

            ])
            ->fetch();
    }

    /**
     * 结算商设置
     */
    public function balance_set(){
        $model = new Balance();
        if ($this->request->isPost()) {

            $data = Request::instance()->only('balance_rate_1,balance_rate_2,balance_rate_3,id');

            $validate = $this->validate($data, 'Balance');
            if ($validate !== true) $this->error($validate);
            if ($model->update_balance($data)) {
                $this->success('操作 成功', url('balance_set'), '_parent_reload');
            } else {
                $this->error('操作失败', url('balance_set'), '_parent_reload');
            }
        }
        $info=$model->find_balance();

        return ZBuilder::make('form')
            ->setPageTitle('结算商设置')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['text', 'balance_rate_1', 'C级结算商兑入率'],
                ['text', 'balance_rate_2', 'B级结算商兑入率'],
                ['text', 'balance_rate_3', 'A级结算商兑入率'],

            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }


}
