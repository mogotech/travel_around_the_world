<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\admin;

use app\common\builder\ZBuilder;
use app\admin\controller\Admin;
use app\world\model\Apply as ApplyModel;
use think\db;
class Apply extends Admin
{

    /**
     * 列表
     * @author 
     */
    public function index()
    {
        $Apply = new ApplyModel();
        $map = $this->getMap();
        $order = $this->getOrder();
        $data_list = Db::view('world_apply a',true)
            ->view('world_user user','username','a.user_id=user.id')
            ->view('world_user p',['username'=>'p_username'],'a.pid=p.id')
            ->where($map)
            ->order($order)
            ->paginate();
        return ZBuilder::make('table')
            ->setPageTitle('反馈列表')// 设置页面标题
            ->setTableName('world_apply')// 设置数据表名
            ->addOrder('id')
            ->hideCheckbox()
            ->setSearch(['p.username','user.username'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '用户账号'],
                ['p_username', '股东账号'],
                ['type', '类型',['反馈','申请抵押','申请配资','申请经济']],
                ['content', '内容','textarea.edit'],
                ['reply', '回复','textarea.edit'],
                ['create_time', '创建时间', 'datetime'],
             ])
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染页面
    }

}