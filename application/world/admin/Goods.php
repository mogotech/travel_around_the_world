<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\world\model\Goods as GoodsModel;
use app\world\model\GoodsPrice;
use think\Db;
use app\world\model\Order;

class Goods extends Admin
{

    /**
     * 资产列表
     * @author 
     */
    public function index()
    {
        $purchase = [
            'title' => '购买记录',
            'icon' => 'glyphicon glyphicon-yen',
            'href' => url('purchase', ['goods_id' => '__id__'])
        ];
        $sell = [
            'title' => '卖出记录',
            'icon' => 'glyphicon glyphicon-usd',
            'href' => url('sell', ['goods_id' => '__id__'])
        ];
        $ransformation = [
            'title' => '互转记录',
            'icon' => 'fa fa-fw fa-arrows-h',
            'href' => url('ransformation', ['goods_id' => '__id__'])
        ];
        $model = new GoodsModel();
        $map = $this->getMap();
        $order = $this->getOrder();
        $order ? '' : $order = 'is_examine,id desc';
        $data_list = $model->get_select($map, $order);


        foreach ($data_list as &$value) {
            $sum = Db::name('world_order')->where(['goods_id' => $value->id])->sum('surplus_sum');
            $sum ? $value->shop_sum = $sum : $value->shop_sum = 0;
        }
        return ZBuilder::make('table')
            ->setPageTitle('资产列表')// 设置页面标题
            ->setTableName('world_goods')// 设置数据表名
            ->addOrder('id')
            ->hideCheckbox()
            ->setSearch(['name', 'nickname'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['nickname', '所属股东'],
                ['name', '资产名称'],
                ['int_money', '初始金额'],
                ['money', '当前金额'],
                ['is_examine', '状态', 'select', ['未审核', '已审核']],
                ['create_time', '创建时间', 'datetime'],
                ['shop_sum', '售出数量'],
                ['right_button', '操作', 'btn']
            ])
//            ->addTopButton('add', [], true)
            ->addRightButton('edit', [], true)
            ->addRightButton('delete')
            ->setRowList($data_list)// 设置表格数据
            ->addRightButton('access', $purchase)//购买记录
            ->addRightButton('access', $sell)// 卖出记录
            ->addRightButton('access', $ransformation)//互转
            ->fetch(); // 渲染页面
    }

    /**
     * 快速编辑
     * @author 
     */
    public function quickEdit($record = [])
    {
        $data = input();
        $goods = GoodsModel::get($data['pk']);
        if ($data['name'] == 'is_examine') {//订单状态
            if ($data['value'] == 0 && $goods->is_examine == 1) $this->error('违规的操作!');
            $goods->is_examine = $data['value'];
            $goods->save();
        }
    }


    /**
     * 添加资产
     * @author 
     */
    public function add()
    {

        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'Goods.admin_add');
            if ($validate !== true) $this->error($validate);
            $model = new GoodsModel();
            if ($model->add_goods($data)) {
                $this->success('新增成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('新增托运单')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'type', 2,],
                ['hidden', 'user_id', 0,],
                ['hidden', 'new_money', 0,],
                ['text', 'name', '资产名称*', ''],
                ['text', 'int_money', '初始金额标准*', ''],
                ['text', 'section', '价格区间*', ''],
                ['text', 'sum', '资产数量*', ''],
                ['switch', 'status', '是否发布', 0],
            ])
            ->fetch();
    }


    /**
     * 编辑
     * @author 
     */
    public function edit($id = null)
    {
        if ($id === null) $this->error('缺少参数');
        $model = new GoodsModel();
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'Goods.edit');
            if ($validate !== true) $this->error($validate);
            if ($model->edit_goods($data)) {
                $this->success('编辑成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }

        $model = new GoodsModel();
        $info = $model->get_find(['id' => $id]);
        return ZBuilder::make('form')
            ->setPageTitle('编辑')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['text', 'name', '资产名称*', ''],
                ['text', 'shop_min', '最小成交金额', ''],
                ['text', 'shop_max', '最大成交金额', ''],
                ['text', 'section', '价格区间*', ''],
                ['radio', 'status', '是否隐藏', '', ['隐藏', '显示'], $info->status],
            ])
            ->setFormData($info)// 设置表单数据
            ->layout([
                'shop_min' => 6, 'shop_max' => 6,
            ])
            ->fetch();
    }


    /**
     * 删除资产
     * @author 
     */
    public function delete($ids = null)
    {
        if ($ids === null) $this->error('缺少参数');
        $data = Goodsmodel::get($ids);
        if ($data->is_examine) $this->error('操作失败,不能删除');
        GoodsModel::destroy($ids);
        $this->success('删除成功');
    }


    /**
     * 购买记录
     * @author 
     */
    public function purchase()
    {
        $id = input('goods_id');
        if (!$id) $this->error('参数不全');
        $OrderModel = new Order();
        $map = $this->getMap();
        $order = $this->getOrder();
        $map['dp_world_order.goods_id'] = $id;
        $map['dp_world_order.is_transfer'] = 0;
        $map['dp_world_order.type'] = 1;
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $OrderModel->get_goods_order_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->setPageTitle('购买记录')// 设置页面标题
            ->setTableName('world_order')// 设置数据表名
            ->addOrder('id')
            ->hideCheckbox()
            ->setSearch(['dp_world_user.username', 'dp_world_user.nickname'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '用户名'],
                ['money', '买入价格'],
                ['actual_sum', '购买数量'],
                ['surplus_sum', '剩余数量'],
                ['status', '状态', ['未审核', '审核成功', '拒绝审核', '用户取消']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 卖出记录
     * @author 
     */
    public function sell()
    {
        $id = input('goods_id');
        if (!$id) $this->error('参数不全');
        $OrderModel = new Order();
        $map = $this->getMap();
        $order = $this->getOrder();
        $map['dp_world_order.goods_id'] = $id;
        $map['dp_world_order.is_transfer'] = 0;
        $map['dp_world_order.type'] = 2;
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $OrderModel->get_goods_order_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->setPageTitle('卖出记录')// 设置页面标题
            ->setTableName('world_order')// 设置数据表名
            ->addOrder('id')
            ->hideCheckbox()
            ->setSearch(['dp_world_user.username', 'dp_world_user.nickname'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '用户名'],
                ['money', '买入价格'],
                ['money2', '卖出价格'],
                ['surplus_sum', '卖出数量'],
                ['status', '状态', ['未审核', '审核成功', '拒绝审核', '用户取消']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 互转
     * @author 
     */
    public function ransformation()
    {
        $id = input('goods_id');
        if (!$id) $this->error('参数不全');
        $OrderModel = new Order();
        $map = $this->getMap();
        $order = $this->getOrder();
        $map['dp_world_order.goods_id'] = $id;
        $map['dp_world_order.is_transfer'] = 1;
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $OrderModel->get_goods_ransformation($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->setPageTitle('互转记录')// 设置页面标题
            ->setTableName('world_order')// 设置数据表名
            ->addOrder('dp_world_order.id')
            ->hideCheckbox()
            ->setSearch(['transfer.p_username', 'transfer.p_nickname', 'dp_world_user.username', 'dp_world_user.nickname'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['p_username', '转出账号'],
                ['p_nickname', '转出用户名'],
                ['actual_sum', '转出数量'],
                ['username', '接收账号'],
                ['nickname', '接收用户名'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面


    }


}