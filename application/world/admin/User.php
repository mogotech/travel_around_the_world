<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：User.php
 * 时间：2017年8月25日
 * 作者：
 */


namespace app\world\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\world\model\User as UserModel;
use app\world\model\NewsUser;
use app\world\model\Mix;
use app\world\model\MixOut;
use app\world\model\Order;
use app\world\model\Trans;
use app\world\model\WalletRuls;
use app\world\model\Mortgage;
use app\world\model\Test;

/**
 * 用户默认控制器
 * @package app\user\admin
 */
class User extends Admin
{
    /**
     * 用户首页
     */
    public function index()
    {
        $btn_access = [
            'title' => '查看下级',
            'icon' => 'fa fa-fw fa-arrow-circle-o-down',
            'href' => url('index', ['parent' => '__id__']),
        ];

        $btn_record = [
            'title' => '查看记录',
            'icon' => 'glyphicon glyphicon-list-alt',
            'href' => url('record', ['id' => '__id__']),
        ];

        $btn_subordinate_record = [
            'title' => '查看下级记录',
            'icon' => 'glyphicon glyphicon-list-alt',
            'href' => url('subordinate_record', ['id' => '__id__']),
        ];


        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $map = $this->getMap();
        if (input('parent')) $map['parent'] = input('parent');
        $order = $this->getOrder();
        $model = new UserModel();
        $data_list = $model->get_select($map, $order, $list_rows);

        $a = 0;
        $b = 0;
        $c = 0;
        foreach ($data_list as &$v) {
            if (is_object($data_list)) {
                $v->sum_money = $model->sum_money($v->id);
                $reference = Test::index($v->id);
                $v->reference_money = $reference['money'] - $v->money2;
                $v->reference_money1 = $reference['money1'];
                $a += $reference['money'];
                $b += $reference['money1'];
            }
            if (is_array($data_list)) {
                $v['sum_money'] = $model->sum_money($v['id']);
                $reference = Test::index($v['id']);
                $v['reference_money'] = $reference['money'] - $v['money2'];
                $v['reference_money1'] = $reference['money1'];
                $a += $reference['money'];
                $b += $reference['money1'];
            }
        }

        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setSearch(['username', 'nickname'])// 设置搜索参数
            ->setPageTitle('用户列表')// 设置页面标题
            ->setTableName('world_user')// 设置数据表名
            ->addFilter('group_id', UserModel::$state)// 添加筛选
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['level', '级别', '', '/'],
                ['username', '账号'],
                ['nickname', '昵称'],
                ['group_id', '分组', 'text', '', UserModel::$state],
                ['money1', '余额'],
                ['reference_money', '参考余额'],
                ['money2', '佣金'],
                ['money4', '资产余额'],
                ['money5', '电子钱包'],
                ['reference_money1', '参考电子钱包'],
                ['money3', '历史佣金'],
                ['sum_money', '总资产'],
                ['status', '是否封号', 'select', ['禁用', '正常']],
                ['is_brokers', '经纪商', 'select', ['不是', '区域经济商', '大区经纪商', '国际经纪商']],
                ['is_capital', '是否配资商', ['不是', '是']],
                ['is_mortgage', '是否抵押商', ['不是', '是']],
//                ['brokers', '是否经纪商', ['不是', '区域经济商', '大区经纪商', '国际经纪商']],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('add', [], true)
            ->addRightButton('edit', [], true)
            ->addRightButton('parent', $btn_access)
            ->addRightButton('record', $btn_record)
            ->addRightButton('subordinate_record', $btn_subordinate_record)
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 添加用户
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'User.admin_add');
            if ($validate !== true) $this->error($validate);
            $model = new UserModel();
            if (!$data['password2']) unset($data['password2']);
            if (!$model->add_user($data)) $this->error('操作失败', url('index'), '_parent_reload');
            $this->success('新增成功', url('index'), '_parent_reload');
        }

        return ZBuilder::make('form')
            ->setPageTitle('新增用户')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'code', ''],
                ['text', 'username', '账号*', ''],
                ['text', 'password1', '密码*(全部权益)',],
                ['text', 'password2', '第二密码(部分权益)',],
                ['text', 'nickname', '昵称',],
                ['hidden', 'pid', '0',],
            ])
            ->fetch();
    }


    /**
     * 编辑用户
     * @author 
     */
    public function edit($id = null)
    {
        if ($id === null) $this->error('缺少参数');
        $model = new UserModel();
        $info = $model->get_find(['id' => $id], 'id,pid,nickname,username,money1,withdrawal_type,account,account_name,bank,acc_bank');
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'User.admin_edit');
            if ($validate !== true) $this->error($validate);
            if (isset($data['edit_money'])) {
                if (number_format($data['get_money'], 2)) {
                    $NewsUser = new NewsUser();
                    $Mix = new Mix();
                    $Mix->user_id = $info->id;
                    $Mix->pid = $model->get_pid($info->id);
                    $Mix->withdrawal_type = 3;
                    $Mix->status = 4;
                    $NewsUser->title = '平臺充值消息';
                    $NewsUser->user_id = $info->id;

                    if ($data['edit_money'] == 'add') {
                        $type=1;
                        $Mix->mix_money = $data['get_money'];
                        $NewsUser->content = '平臺向您充值了' . $data['get_money'] . '美金';
                        $data['money1'] = $info->money1 + (float)($data['get_money']);
                    }
                    if ($data['edit_money'] == 'reduce') {
                        $type=0;
                        $Mix->mix_money = -$data['get_money'];
                        $NewsUser->content = '平臺向您充值了' . -$data['get_money'] . '美金';
                        $data['money1'] = $info->money1 - (float)($data['get_money']);
                    }
                    $Mix->save();
                    $NewsUser->save();
                }
            }
            if ($data['password1']) $data['password1'] = $model->add_password($data['password1']); else unset($data['password1']);
            if ($data['password2']) $data['password2'] = $model->add_password($data['password2']); else unset($data['password2']);
            if ($model->edit_user($data)) {
                if(isset($type))UserModel::money_change($info->id,$type,$data['get_money'],'後臺充值 Backstage recharge');
                $this->success('编辑成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑用户')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['static', 'username', '账号',],
                ['text', 'nickname', '昵称',],
                ['static', 'money1', '余额',],
                ['radio', 'edit_money', '充值', '', ['add' => '增加余额', 'reduce' => '减少余额']],
                ['text', 'get_money', '充值余额',],
                ['text', 'password1', '密码(全部权益)', '不填则不修改'],
                ['text', 'password2', '第二密码(部分权益)', '不填则不修改'],
                ['select', 'withdrawal_type', '支付方式', '', UserModel::$withdrawal_type,],
                ['text', 'account', '提现账号'],
                ['text', 'account_name', '提现姓名'],
                ['text', 'bank', '提现银行'],
                ['text', 'acc_bank', '开户支行'],
            ])
            ->setFormData($info)// 设置表单数据
            ->setTrigger('withdrawal_type', '1', 'bank,acc_bank')
            ->setTrigger('edit_money', 'add,reduce', 'get_money')
            ->fetch();
    }


    /**
     * 删除用户
     * @author 
     */
    public function delete($ids = null)
    {
        if ($ids === null) $this->error('缺少参数');
        if (is_array($ids)) {
            foreach ($ids as $k => $v) {
                UserModel::destroy($v);
            }
        } else {
            UserModel::destroy($ids);
        }
        $this->success('删除成功');
    }


    /**
     * 用户记录列表
     * @author 
     */
    public function record()
    {
        $id = input('id') ? input('id') : $_SESSION['admin']['id'];
        $_SESSION['admin']['id'] = $id;
        if (!$id) $this->error('参数不全');
        $this->assign([
            'id' => $id,
        ]);
        return $this->fetch();
    }

    /**
     * 用户兑入记录列表
     * @author 
     */
    public function mix_record()
    {
        if (!input('id')) $this->error('参数不全');
        $map = $this->getMap();
        $map['user_id'] = input('id');
        $MixModel = new Mix();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $MixModel->get_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('兑入记录')// 设置页面标题
            ->setTableName('world_mix')// 设置数据表名
            ->addFilter('status', ['申请兑入', '等待兑入', '兑入完成', '匹配失败', '后台充值'])// 添加筛选
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['withdrawal_type', '充值方式', ['支付宝', '银行卡', '微信', '平台']],
                ['account', '兑入账号'],
                ['account_name', '兑入姓名'],
                ['bank', '开户银行'],
                ['acc_bank', '开户支行'],
                ['mix_money', '兑入美金'],
                ['exchange', '美金汇率'],
                ['money', '兑入人民币'],
                ['status', '状态', ['申请兑入', '等待兑入', '兑入完成', '匹配失败', '后台充值']],
                ['create_time', '创建时间', 'datetime'],

            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 用户兑出记录列表
     * @author 
     */
    public function mixout_record()
    {
        if (!input('id')) $this->error('参数不全');
        $map = $this->getMap();
        $map['user_id'] = input('id');
        $MixModel = new MixOut();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $MixModel->get_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('兑出记录')// 设置页面标题
            ->setTableName('world_mix_out')// 设置数据表名
            ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['withdrawal_type', '兑出方式', ['支付宝', '银行卡', '微信']],
                ['account', '充值账号'],
                ['account_name', '充值姓名'],
                ['bank', '开户银行'],
                ['acc_bank', '开户支行'],
                ['mix_money', '兑出美金'],
                ['exchange', '美金汇率'],
                ['fee_money', '手续费'],
                ['money', '兑出人名币'],
                ['status', '状态', ['申请兑出', '完成兑出', '匹配失败']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 资产买卖记录
     * @author 
     */
    public function order_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Order = new Order();
        $map = $this->getMap();
        if (input('_select_field')) $map['world_order.type'] = input('_select_value'); else $map['world_order.type'] = 1;
        $map['world_order.user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Order->get_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('资产买卖记录')// 设置页面标题
            ->setTableName('world_mixout')// 设置数据表名
            ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['goods_name', '资产名称'],
                ['sum', '申请购买数量'],
                ['actual_sum', '买入数量'],
                ['surplus_sum', '剩余数量'],
                ['goods_money', '当前价格'],
                ['money', '买入价格'],
                ['money2', '卖出价格'],
                ['is_transfer', '是否收入', ['否', '是']],
                ['status', '状态', ['匹配中', '匹配成功', '匹配失败', '用户取消']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->addTopSelect('world_order.type', '请选择', ['1' => '买入', '2' => '卖出'], 1)
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 余额互转记录
     * @author 
     */
    public function trans_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Trans = new Trans();
        $map = $this->getMap();
        if (input('_select_field')) $type = input('_select_value'); else $type = 1;
        $map['user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Trans->get_list($map['user_id'], $type, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('余额互转记录')// 设置页面标题
            ->setTableName('world_trans')// 设置数据表名
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '转出/收入账户'],
                ['nickname', '转出/收入昵称'],
                ['money', '转出/收入金额'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->addTopSelect('world_order.type', '请选择', ['1' => '转出', '2' => '收入'], 1)
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面

    }


    /**
     * 资产互转记录
     * @author 
     */
    public function goods_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Order = new Order();
        $map = $this->getMap();
        if (input('_select_field')) $type = input('_select_value'); else $type = 1;
        $map['user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Order->get_record($map['user_id'], $type, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('资产互转记录')// 设置页面标题
            ->setTableName('world_trans')// 设置数据表名
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '转出/收入账户'],
                ['nickname', '转出/收入昵称'],
                ['name', '转出/收入资产名称'],
                ['sum', '转出/收入资产数量'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->addTopSelect('world_order.type', '请选择', ['1' => '转出', '2' => '收入'], 1)
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 电子钱包
     * @author 
     */
    public function wallet_record()
    {
        if (!input('id')) $this->error('参数不全');
        $WalletRuls = new WalletRuls();
        $map = $this->getMap();
        if (input('_select_field')) $type = input('_select_value'); else $type = 1;
        $map['user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $WalletRuls->get_all($map);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('电子钱包')// 设置页面标题
            ->setTableName('world_trans')// 设置数据表名
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['money', '原钱包金额'],
                ['add_money', '增加金额'],
                ['proportion', '增加比例'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 下级用户记录列表（股东）
     * @author 
     */
    public function subordinate_record()
    {
        $id = input('id') ? input('id') : $_SESSION['subordinate_record']['id'];
        if (!$id) $this->error('参数不全');
        $UserModel = new UserModel();
        $user = $UserModel->get_find(['id' => $id]);
        if ($user->group_id != 2) $this->error('该用户不是股东');
        $_SESSION['subordinate_record']['id'] = $id;
        $this->assign([
            'id' => $id,
        ]);
        return $this->fetch();
    }


    /**
     * 下级用户兑入记录列表（股东）
     * @author 
     */
    public function subordinate_mix_record()
    {
        if (!input('id')) $this->error('参数不全');
        $map = $this->getMap();
        $map['world_mix.pid'] = input('id');
        $MixModel = new Mix();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $MixModel->get_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('兑入记录')// 设置页面标题
            ->setTableName('world_mix')// 设置数据表名
            ->addFilter('status', ['申请兑入', '等待兑入', '兑入完成', '匹配失败', '后台充值'])// 添加筛选
            ->setSearch(['world_user.username' => '账号', 'world_user.nickname' => '昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '昵称'],
                ['withdrawal_type', '充值方式', ['支付宝', '银行卡', '微信', '平台']],
                ['account', '兑入账号'],
                ['account_name', '兑入姓名'],
                ['bank', '开户银行'],
                ['acc_bank', '开户支行'],
                ['mix_money', '兑入美金'],
                ['exchange', '美金汇率'],
                ['money', '兑入人民币'],
                ['status', '状态', ['申请兑入', '等待兑入', '兑入完成', '匹配失败', '后台充值']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 用户兑出记录列表
     * @author 
     */
    public function subordinate_mixout_record()
    {
        if (!input('id')) $this->error('参数不全');
        $map = $this->getMap();
        $map['world_mix_out.pid'] = input('id');
        $MixModel = new MixOut();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $MixModel->get_list($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('兑出记录')// 设置页面标题
            ->setTableName('world_mix_out')// 设置数据表名
            ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
            ->setSearch(['world_user.username' => '账号', 'world_user.nickname' => '昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '昵称'],
                ['withdrawal_type', '兑出方式', ['支付宝', '银行卡', '微信']],
                ['account', '充值账号'],
                ['account_name', '充值姓名'],
                ['bank', '开户银行'],
                ['acc_bank', '开户支行'],
                ['mix_money', '兑出美金'],
                ['exchange', '美金汇率'],
                ['fee_money', '手续费'],
                ['money', '兑出人名币'],
                ['status', '状态', ['申请兑出', '完成兑出', '匹配失败']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 资产买卖记录
     * @author 
     */
    public function subordinate_order_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Order = new Order();
        $map = $this->getMap();
        $map['world_order.pid'] = input('id');
        $map['world_order.is_transfer'] = 0;
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Order->get_subordinate_list($map, $order, $list_rows);
        if (@$map['world_order.type'] == 1) { //买入
            return ZBuilder::make('table')
                ->addOrder('id')// 添加排序
                ->hideCheckbox()
                ->setPageTitle('资产买卖记录')// 设置页面标题
                ->setTableName('world_mixout')// 设置数据表名
                ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
                ->setSearch(['world_user.username', 'world_user.nickname', 'world_goods.name' => '资产名称'])// 设置搜索参数
                ->addColumns([ // 批量添加列
                    ['id', 'ID'],
                    ['username', '账号'],
                    ['nickname', '用户名'],
                    ['goods_name', '资产名称'],
                    ['actual_sum', '买入数量'],
                    ['surplus_sum', '剩余数量'],
                    ['goods_money', '当前价格'],
                    ['money', '买入价格'],
                    ['purchase_fee', '买入手續費(百分比)'],
                    ['status', '状态', ['匹配中', '匹配成功', '匹配失败', '用户取消']],
                    ['create_time', '创建时间', 'datetime'],
                ])
                ->addTopSelect('world_order.type', '请选择', ['1' => '买入', '2' => '卖出'], 1)
                ->setRowList($data)// 设置表格数据
                ->fetch(); // 渲染页面
        } elseif (@$map['world_order.type'] == 2) {
            return ZBuilder::make('table')
                ->addOrder('id')// 添加排序
                ->hideCheckbox()
                ->setPageTitle('资产买卖记录')// 设置页面标题
                ->setTableName('world_mixout')// 设置数据表名
                ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
                ->setSearch(['world_user.username', 'world_user.nickname', 'world_goods.name' => '资产名称'])// 设置搜索参数
                ->addColumns([ // 批量添加列
                    ['id', 'ID'],
                    ['username', '账号'],
                    ['nickname', '用户名'],
                    ['goods_name', '资产名称'],
                    ['actual_sum', '卖出数量'],
                    ['money', '买入价格'],
                    ['money2', '卖出价格'],
                    ['sell_fee', '卖出手續費(百分比)'],
                    ['status', '状态', ['匹配中', '匹配成功', '匹配失败', '用户取消']],
                    ['create_time', '创建时间', 'datetime'],
                ])
                ->addTopSelect('world_order.type', '请选择', ['1' => '买入', '2' => '卖出'], 1)
                ->setRowList($data)// 设置表格数据
                ->fetch(); // 渲染页面
        }
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('资产买卖记录')// 设置页面标题
            ->setTableName('world_mixout')// 设置数据表名
            ->addFilter('status', ['申请兑出', '完成兑出', '匹配失败'])// 添加筛选
            ->setSearch(['world_user.username', 'world_user.nickname', 'world_goods.name' => '资产名称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '用户名'],
                ['goods_name', '资产名称'],
                ['actual_sum', '买入数量'],
                ['surplus_sum', '剩余数量'],
                ['goods_money', '当前价格'],
                ['money', '买入价格'],
                ['purchase_fee', '买入手續費(百分比)'],
                ['status', '状态', ['匹配中', '匹配成功', '匹配失败', '用户取消']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->addTopSelect('world_order.type', '请选择', ['1' => '买入', '2' => '卖出'], 1)
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 余额互转记录
     * @author 
     */
    public function subordinate_trans_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Trans = new Trans();
        $map = $this->getMap();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $UserModel = new UserModel();
        $user_all = collection($UserModel->select())->toArray();
        foreach ($user_all as $val) {
            $user_all1[$val['id']] = $val;
        }
        foreach ($user_all1 as $k => $v) {
            if (@$user_all1[$v['pid']]) {
                $user_all1[$v['pid']]['sub'][$k] = $v;
            }
        }
        $info = array();
        $UserModel->get_a(input('id'), $user_all1, $info);
        $ids = '';
        foreach ($info as $v) {
            $ids = $ids . $v['id'] . ',';
        }
        $ids = rtrim($ids, ',');
        $type = @$map['world_order.type'];
        if ($type == 1) {      //转出
            $map['dp_world_trans.user_id'] = ['in', $ids];
        } elseif ($type == 2) { //收入
            $map['dp_world_trans.receive_id'] = ['in', $ids];
        }
        unset($map['world_order.type']);
        unset($map['type']);
        $data = $Trans->get_subordinate_list($map, $type, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('余额互转记录')// 设置页面标题
            ->setTableName('world_trans')// 设置数据表名
            ->setSearch(['user.username' => '转出账户', 'user.nickname' => '转出昵称', 'receive.username' => '收入账户', 'receive.nickname' => '收入昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '转出账户'],
                ['nickname', '转出昵称'],
                ['money', '转出/收入金额'],
                ['r_username', '收入账户'],
                ['r_nickname', '收入昵称'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->addTopSelect('world_order.type', '请选择', ['1' => '转出', '2' => '收入'], 1)
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 资产互转记录
     * @author 
     */
    public function subordinate_goods_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Order = new Order();
        $map = $this->getMap();
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $UserModel = new UserModel();
        $user_all = collection($UserModel->select())->toArray();
        foreach ($user_all as $val) {
            $user_all1[$val['id']] = $val;
        }
        foreach ($user_all1 as $k => $v) {
            if (@$user_all1[$v['pid']]) {
                $user_all1[$v['pid']]['sub'][$k] = $v;
            }
        }
        $info = array();
        $UserModel->get_a(input('id'), $user_all1, $info);
        $ids = '';
        foreach ($info as $v) {
            $ids = $ids . $v['id'] . ',';
        }
        $ids = rtrim($ids, ',');
        $map['dp_world_order.transfer_id'] = ['in', $ids];
        $map['dp_world_order.is_transfer'] = 1;
        $data = $Order->get_subordinate_record($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('资产互转记录')// 设置页面标题
            ->setTableName('world_wallet')// 设置数据表名
            ->setSearch(['dp_world_goods.name' => '资产名称', 'transfer.username' => '转出账户', 'transfer.nickname' => '转出昵称', 'user.username' => '收入账户', 'user.nickname' => '收入昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['p_username', '转出账户'],
                ['p_nickname', '转出昵称'],
                ['name', '转出/收入资产名称'],
                ['sum', '转出/收入资产数量'],
                ['username', '收入账户'],
                ['nickname', '收入昵称'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 下级电子钱包
     * @author 
     */
    public function subordinate_wallet_record()
    {
        if (!input('id')) $this->error('参数不全');
        $WalletRuls = new WalletRuls();
        $map = $this->getMap();
        if (input('_select_field')) $type = input('_select_value'); else $type = 1;
        $map['user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $WalletRuls->get_vive($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('电子钱包')// 设置页面标题
            ->setTableName('world_wallet')// 设置数据表名
            ->setSearch(['username' => '账号', 'nickname' => '昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '账号'],
                ['nickname', '昵称'],
                ['money', '原钱包金额'],
                ['add_money', '增加金额'],
                ['proportion', '增加比例'],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }

    /**
     * 抵押记录
     * @author 
     */
    public function mortgage_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Mortgage = new Mortgage();
        $map = $this->getMap();
        $map['mortgage.user_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Mortgage->get_all($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('抵押记录')// 设置页面标题
            ->setTableName('world_mortgage')// 设置数据表名
            ->setSearch(['mortgage_user.username' => '出借人账号', 'mortgage_user.nickname' => '出借人昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['mortgage_username', '出借人账号'],
                ['mortgage_nickname', '出借人昵称'],
                ['goods_name', '商品名称'],
                ['goods_money', '商品价值'],
                ['sum', '抵押数量'],
                ['day', '抵押天数'],
                ['money', '抵押金额'],
                ['mortgage', '利率'],
                ['status', '状态', ['审核中', '抵押中', '已赎回', '已典当', '已取消', '审核拒绝']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 下级抵押记录
     * @author 
     */
    public function subordinate_mortgage_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Mortgage = new Mortgage();
        $map = $this->getMap();

        $UserModel = new UserModel();
        $user_all = collection($UserModel->select())->toArray();
        foreach ($user_all as $val) {
            $user_all1[$val['id']] = $val;
        }
        foreach ($user_all1 as $k => $v) {
            if (@$user_all1[$v['pid']]) {
                $user_all1[$v['pid']]['sub'][$k] = $v;
            }
        }
        $info = array();
        $UserModel->get_a(input('id'), $user_all1, $info);
        $ids = '';
        foreach ($info as $v) {
            $ids = $ids . $v['id'] . ',';
        }
        $ids = rtrim($ids, ',');
        $map['mortgage.user_id'] = ['in', $ids];
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Mortgage->get_all($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('抵押人记录')// 设置页面标题
            ->setTableName('world_mortgage')// 设置数据表名
            ->setSearch(['mortgage_user.username' => '出借人账号', 'mortgage_user.nickname' => '出借人昵称', 'user.username' => '抵押人账号', 'user.nickname' => '抵押人昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '抵押人账号'],
                ['nickname', '抵押人昵称'],
                ['mortgage_username', '出借人账号'],
                ['mortgage_nickname', '出借人昵称'],
                ['goods_name', '商品名称'],
                ['goods_money', '商品价值'],
                ['sum', '抵押数量'],
                ['day', '抵押天数'],
                ['money', '抵押金额'],
                ['mortgage', '利率'],
                ['status', '状态', ['审核中', '抵押中', '已赎回', '已典当', '已取消', '审核拒绝']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 出借记录
     * @author 
     */
    public function lend_record()
    {
        if (!input('id')) $this->error('参数不全');
        $Mortgage = new Mortgage();
        $map = $this->getMap();
        $map['mortgage.mortgage_id'] = input('id');
        $order = $this->getOrder();
        $list_rows = input('list_rows') ? input('list_rows') : 10;
        $data = $Mortgage->get_all($map, $order, $list_rows);
        return ZBuilder::make('table')
            ->addOrder('id')// 添加排序
            ->hideCheckbox()
            ->setPageTitle('抵押记录')// 设置页面标题
            ->setTableName('world_mortgage')// 设置数据表名
            ->setSearch(['user.username' => '抵押人账号', 'user.nickname' => '抵押人昵称'])// 设置搜索参数
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['username', '抵押人账号'],
                ['nickname', '抵押人昵称'],
                ['goods_name', '商品名称'],
                ['goods_money', '商品价值'],
                ['sum', '抵押数量'],
                ['day', '抵押天数'],
                ['money', '抵押金额'],
                ['mortgage', '利率'],
                ['status', '状态', ['审核中', '抵押中', '已赎回', '已典当', '已取消', '审核拒绝']],
                ['create_time', '创建时间', 'datetime'],
            ])
            ->setRowList($data)// 设置表格数据
            ->fetch(); // 渲染页面
    }

//    /**
//     * 配资记录
//     * @author 
//     */
//    public function capital()
//    {
//        if (!input('id')) $this->error('参数不全');
//        $OrderModel = new Order();
//        $id = input('id');
//        $map['world_order.user_id'] = $id;
//        $map['world_order.type'] = 4;
//        $map['world_order.status'] = ['in', [0, 2, 3, 4, 5, 6]];
//        $map['world_order.surplus_sum'] = 0;
//        $data1 = $OrderModel->get_list($map, '', '', 1);
//        if ($data1) $data1 = collection($data1)->toArray();
//        $where['world_order.user_id'] = $id;
//        $where['world_order.type'] = 4;
//        $where['world_order.status'] = 1;
//        $where['world_order.surplus_sum'] = ['>', 0];
//        $data2 = $OrderModel->get_list($where, '', '', 1);
//        if ($data2) $data2 = collection($data2)->toArray();
//        $data = array_merge($data1, $data2);
//        $mp['world_order.user_id'] = $id;
//        $mp['world_order.type'] = 5;
//        $mp['world_order.status'] = ['in', [0, 1]];
//        $data3 = $OrderModel->get_list($mp, '', '', 1);
//        if ($data3) $data3 = collection($data3)->toArray();
//        if ($data3) {
//            foreach ($data3 as &$value) {
//                $order = Order::find($value['order_id']);
//                $value['create_time'] = $order->create_time;
//            }
//        }
//
//        $data = array_merge($data, $data3);
//        if ($data) {
//            $data = my_sort($data, 'id', SORT_DESC);
//        }
//        foreach ($data as $k => &$v) {
//            $info = $OrderModel->get_capital($v['id']);
//            $v['past_time'] = $info['past_time'];
//            $v['redeem_money'] = $info['redeem_money'];
//        }
//        return ZBuilder::make('table')
//            ->addOrder('id')// 添加排序
//            ->hideCheckbox()
//            ->setPageTitle('配资记录')// 设置页面标题
//            ->addColumns([ // 批量添加列
//                ['id', 'ID'],
//                ['goods_name', '商品名称'],
//                ['nickname', '抵押人昵称'],
//                ['redeem_money', '应付手续费'],
//                ['past_time', '抵押天数'],
//                ['selling_money', '配资金额'],
//                ['capital_pro', '融资比例'],
//                ['capital_interest', '日利率'],
//                ['create_time', '创建时间', 'datetime'],
//            ])
//            ->setRowList($data)// 设置表格数据
//            ->fetch(); // 渲染页面
//
//    }


}
