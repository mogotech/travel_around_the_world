<?php
/**
 * Created by PhpStorm.
 * User: ou
 * Date: 2018/3/29
 * Time: 14:24
 */
namespace app\world\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\world\model\User as UserModel;
use app\world\model\NewsUser;
use app\world\model\Mix;
use app\world\model\MixOut;
use app\world\model\Order;
use app\world\model\Trans;
use app\world\model\WalletRuls;
use app\world\model\Mortgage;
use app\world\model\Notice as NoticeModel;

/**
 * 公告默认控制器
 * @package app\user\admin
 */
class Notice extends Admin
{
    /**
     * 消息列表
     * @author
     */
    public function index()
    {
        $model = new NoticeModel();
        $map = $this->getMap();
        $order = $this->getOrder();
        $data_list = $model->get_select($map, $order);
        $user = UserModel::where(['group_id' => 2])->column('nickname', 'id');
        $user[0] = '所有人';
        return ZBuilder::make('table')
            ->setPageTitle('消息列表')// 设置页面标题
            ->setTableName('world_news')// 设置数据表名
            ->addOrder('id')
            ->hideCheckbox()
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['pid', '发送股东组', 'text', '', $user],
                ['type', '类型', 'text', '', [1 => '内链', 2 => '外链']],
                ['title', '标题'],
                ['content', '内容'],
                ['link', '链接'],
                ['create_time', '创建时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('add', [], true)
            ->addRightButton('edit', [], true)
            ->addRightButton('delete')
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染页面
    }
    /**
     * 添加消息
     * @author
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'News.add');
            if ($validate !== true) $this->error($validate);
            $model = new NoticeModel();
            if (!$model->add_news($data)) $this->error('操作失败', url('index'), '_parent_reload');
            $this->success('新增成功', url('index'), '_parent_reload');
        }

        $user = UserModel::where(['group_id' => 2])->column('nickname', 'id');
        $user[0] = '所有人';
        return ZBuilder::make('form')
            ->setPageTitle('新增用户')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['select', 'pid', '发送人*', '', $user, '0'],
                ['select', 'type', '消息类型*', '', ['1' => '内链', '2' => '外链'], '1'],
                ['text', 'link', '请输入链接*', '链接必须带上http://,例如http://www.baidu.com'],
                ['text', 'title', '标题*', ''],
                ['ueditor', 'content', '内容',],
            ])
            ->setTrigger('type', '2', 'link')
            ->fetch();
    }


    /**
     * 编辑
     * @author
     */
    public function edit($id = null)
    {
        if ($id === null) $this->error('缺少参数');
        $model = new NoticeModel();
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'News.edit');
            if ($validate !== true) $this->error($validate);
            if ($model->edit($data)) {
                $this->success('编辑成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }
        $info = $model->get_find(['id' => $id]);
        return ZBuilder::make('form')
            ->setPageTitle('编辑托运单')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['select', 'type', '消息类型*', '', ['1' => '内链', '2' => '外链'], '1'],
                ['text', 'link', '请输入链接*', '链接必须带上http://,例如http://www.baidu.com'],
                ['text', 'title', '标题*', ''],
                ['ueditor', 'content', '内容',],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

    /**
     * 删除用户
     * @author
     */
    public function delete($ids = null)
    {
        if ($ids === null) $this->error('缺少参数');
        $model = new NoticeModel();
        $model->deletes($ids);
        $this->success('删除成功');

    }
}