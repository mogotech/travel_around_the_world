<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\admin;

use app\common\builder\ZBuilder;
use app\admin\controller\Admin;
use app\world\model\GoodsRuls as GoodsRulsModel;

class GoodsRuls extends Admin
{

    /**
     * 初始化
     * @author 
     */
    protected function _initialize()
    {
        parent::_initialize();
        $goods_id = input('goods_id');
        $this->goods_id = $goods_id;
        if (!$this->goods_id) $this->error('参数不全');
    }

    //资产id
    protected $goods_id = null;


    /**
     * 资产价格节点列表
     * @author 
     */
    public function index()
    {

        $model = new GoodsRulsModel();
        $map = $this->getMap();
        $map['goods_id'] = $this->goods_id;
        $order = $this->getOrder();
        $data_list = $model->get_select($map, $order);
        return ZBuilder::make('table')
            ->setPageTitle('资产价格节点列表')// 设置页面标题
            ->setTableName('world_goods_ruls')// 设置数据表名
            ->addOrder('id,time')
            ->addColumns([ // 批量添加列
                ['id', 'ID'],
                ['money', '价格'],
                ['times', '时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('add', ['href' => url('add', ['goods_id' => $this->goods_id])], true)
            ->addRightButton('edit', ['href' => url('edit', ['goods_id' => $this->goods_id, 'id' => '__id__'])], true)
            ->addRightButtons('delete')// 批量添加右侧按钮
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染页面
    }


    /**
     * 添加价格节点
     * @author 
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'GoodsRuls.admin_add');
            if ($validate !== true) $this->error($validate);
            $model = new GoodsRulsModel();
            if ($model->add_goods_ruls($data)) {
                $this->success('新增成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增托运单')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'goods_id', $this->goods_id,],
                ['text', 'money', '金额'],
                ['datetime', 'times', '时间'],

            ])
            ->fetch();
    }


    /**
     * 编辑运单
     * @author 
     */
    public function edit($id = null)
    {
        if ($id === null) $this->error('缺少参数');
        $model = new GoodsRulsModel();
        if ($this->request->isPost()) {
            $data = input();
            $validate = $this->validate($data, 'GoodsRuls.admin_edit');
            if ($validate !== true) $this->error($validate);
            if ($model->edit_goods_ruls($data)) {
                $this->success('编辑成功', url('index'), '_parent_reload');
            } else {
                $this->error('操作失败', url('index'), '_parent_reload');
            }
        }


        $info = $model->get_find(['id' => $id]);
        return ZBuilder::make('form')
            ->setPageTitle('编辑托运单')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['hidden', 'goods_id', $this->goods_id,],
                ['text', 'money', '金额'],
                ['datetime', 'times', '时间'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }


    /**
     * 删除
     * @author 
     */
    public function delete($ids = null)
    {
        if ($ids === null) $this->error('缺少参数');
        if (is_array($ids)) {
            foreach ($ids as $k => $v) {
                GoodsRulsModel::destroy($v);
            }
        } else {
            GoodsRulsModel::destroy($ids);
        }
        $this->success('删除成功');
    }


}