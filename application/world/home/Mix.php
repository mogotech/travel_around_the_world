<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\model\Balance;
use app\world\model\Configs;
use app\world\model\User as UserModel;
use app\world\model\Mix as MixModel;
use app\world\model\NewsUser;

class Mix extends Common
{

//    /**
//     * 兑入
//     * @author 
//     */
//    public function index()
//    {
//        $Configs = new Configs();
//        $configs_data = $Configs->get_find()->toArray();
//        if ($this->request->post()) {
//            $User = new UserModel();
//            $Mix = new MixModel();
//            $NewsUser = new NewsUser();
//            if (!is_numeric(input('mix_money')) && !input('mix_money') > 0) $this->error('请输入正确的金额');
//            $Mix->mix_money = floatval(input('mix_money'));
//            $Mix->pid = $User->get_pid(session('home_user')['id']);
//            $Mix->user_id = session('home_user')['id'];
//            $Mix->exchange = $configs_data['exchange'];
//            $Mix->money = $Mix->exchange * $Mix->mix_money;
//            $Mix->save();
//            $NewsUser->user_id = session('home_user')['id'];
//            $NewsUser->title = '兑入消息';
//            $NewsUser->content = '您的兑入申请已发出,请等待后台匹配交易会员,详情查看兑入记录';
//            $NewsUser->save();
//            $this->success('操作成功');
//        }
//        $this->assign([
//            'configs_data' => $configs_data,
//        ]);
//        return $this->fetch();
//    }


    /**
     * 兑入
     * @author 
     */
    public function index()
    {
        $Configs = new Configs();
        $configs_data = $Configs->get_find()->toArray();
        $Balance=new Balance();
        $balance_set=$Balance->find_balance();

        $User = new UserModel();
        $user=$User->get_find(['id'=>session('home_user')['id']],'id,pid,balance_level,group_id,is_balance');//获取当前用户信息
        $balance_data=array();

        $balance_data['balance_level']=$Balance->get_balance_level($user['balance_level']);
        if($user['is_balance']==1) {
            $balance_data['balance_rate'] = $balance_set['balance_rate_' . $user['balance_level']];
        }else{
            $balance_data['balance_rate'] =1;
        }
        $balance_data['is_balance']=$user['is_balance'];


        if ($this->request->post()) {
            $User = new UserModel();
            $Mix = new MixModel();
            $NewsUser = new NewsUser();
            if (!is_numeric(input('mix_money')) && !input('mix_money') > 0) return ['code' => 0, 'msg' => '請輸入正確的金額', 'msg1' => 'Please enter the correct amount', 'content' => ''];
            $Mix->mix_money = floatval(input('mix_money'));
//            $Mix->pid = $User->get_pid(session('home_user')['id']);//这里计算上级的pid
            $Balance=new Balance();
            $user=$User->get_find(['id'=>session('home_user')['id']],'id,pid,balance_level,group_id');//获取当前用户信息

            $father=$Balance->get_father_balance($user);//获取上级结算商信息

            $Mix->pid =$father['id'];//兑入的上级变成了  当前用户上级结算商 或者 股东
            $Mix->user_id = session('home_user')['id'];
            $Mix->exchange = $configs_data['exchange'];
            $Mix->money = $Mix->exchange * $Mix->mix_money;
            $Mix->save();
            $NewsUser->user_id = session('home_user')['id'];
            $NewsUser->title = '充值消息。Add in message';
            $NewsUser->content = '您的消息已發出，正在匹配餘額賣家。Your message has been sent, and the balance seller is matching';
            $NewsUser->save();
            return ['code' => 0, 'msg' => '您的消息已發出，正在匹配餘額賣家', 'msg1' => 'Your message has been sent, and the balance seller is matching', 'content' => ''];
        }

        $this->assign([
            'configs_data' => $configs_data,
            'balance_data'=>$balance_data
        ]);
        return $this->fetch();
    }


    /**
     * 兑入申请
     * @author 
     */
    public function mix_apply_list()
    {
        $Mix = new MixModel();
        if (is_numeric(input('status'))) $search['world_mix.status'] = input('status');
        if (input('username')) $search['world_user.username'] = input('username');
        $search['world_mix.pid'] = session('home_user')['id'];
        $data = $Mix->get_list($search, 'id desc', 15);

        $this->assign([
            'data' => $data,
            'search' => $search,
        ]);
        return $this->fetch();
    }


    /**
     * 兑入申请详情
     * @author 
     */
    public function mix_apply_main()
    {
        $id = input('id');
        if (!$id) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $Mix = new MixModel();
        $User = new UserModel();
        $data = $Mix->get_find(['world_mix.id' => $id]);
        $user=$User->get_find(['id'=>$data['user_id']],'is_balance,balance_level');
        $Balance=new Balance();//新增结算商详情
        $balance_set=$Balance->find_balance();
        if($user['is_balance']==1&&$user['balance_level']!=0){
            $data['balance_rate']=$balance_set['balance_rate_'.$user['balance_level']];
            $data['balance_name']=$Balance->get_balance_level($user['balance_level']);
            $data['balance_money']=number_format($data['money']*$data['balance_rate'],2);
        }
        $data['is_balance']=$user['is_balance'];
//       echo json_encode($data);die;
        $user_list = collection($User->get_select(['pid' => session('home_user')['id'], 'group_id' => 1]))->toArray(); //查询所有小号
        if ($this->request->post()) {
            $NewsUser = new NewsUser();
            $info = input();

            if ($info['status'] == 1) {
                if (!$info['select']) $this->error(@$_SESSION['is_language'] ? '請選擇充值賬號' : 'Please select recharge account');
                foreach ($user_list as $v) {
                    if ($v['id'] == $info['select']) {
                        $user_data = $v;
                        break;
                    }
                }
                $NewsUser->content = '您的充值申請匹配餘額賣家成功，請到充值記錄查看。Your matching application matches the balance seller\'s success, please go to the entry record view';
                $data->mix_id = $user_data['id'];
                $data->withdrawal_type = $user_data['withdrawal_type'];
                $data->account = $user_data['account'];
                $data->account_name = $user_data['account_name'];
                $data->bank = $user_data['bank'];
                $data->acc_bank = $user_data['acc_bank'];
                $data->is_see = 0;
            }
            if ($info['status'] == 2) {
                if ($data->status != 1) {
                    $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
                }
                $p_user = UserModel::get(session('home_user')['id']);
                if ($p_user->money1 < $data->mix_money) $this->error(@$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low');
                $p_user->money1 -= $data->mix_money;
                $p_user->save();
                $users = $User->get_find(['id' => $data->user_id]);
                $users->money1 += $data->mix_money;
                $users->save();

                $NewsUser->content = '您的充值金額已收到,系統將在24小時内完成充值,請到充值記錄查看。Your exchange amount has been received, the system will be completed within 24 hours, please go to the entry record view';
                UserModel::money_change($p_user->id,0,$data->mix_money,'充值 Recharge');
                UserModel::money_change($users->id,1,$data->mix_money,'充值 Recharge');
            }
            if ($info['status'] == 3) $NewsUser->content = '您的充值申請匹配餘額賣家失敗，請到充值記錄查看。If your matching application matches the balance, the seller fails. Please go to the entry record';

            $NewsUser->title = '充值消息。Add in message';
            $NewsUser->user_id = $data->user_id;
            $data->status = $info['status'];
            $data->is_see = 0;
            $data->save();
            $NewsUser->save();
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign([
            'data' => $data,
            'user_list' => $user_list,
        ]);
        return $this->fetch();
    }


    /**
     * 兑入记录列表
     * @author 
     */
    public function rechargelist()
    {
        if (input('username')) {
            $user = UserModel::get(['username' => input('username')]);
            if (!$user) $this->error(@$_SESSION['is_language'] ? '參數不正確' : 'Missing parameters');
        }
        isset($user) ? $map['user_id'] = $user->id : $map['user_id'] = session('home_user')['id'];
        $MixModel = new MixModel();

        $Balance=new Balance();//新增结算商详情
        $balance_data=$Balance->get_user_balance_msg();

        $data = $MixModel->get_list($map, 'id desc', 15);

        $this->assign([
            'data' => $data,
            'user' => @$user,
            'balance'=>$balance_data
        ]);
        return $this->fetch();
    }

    /**
     * 兑入记录详情
     * @author 
     */
    public function rechargedetails()
    {
        $id = input('id');
        if (!$id) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $MixModel = new MixModel();
        $data = $MixModel->get_find(['world_mix.id' => $id]);
        $data->is_see = 1;
        $data->save();
        $Balance=new Balance();//新增结算商详情
        $balance_data=$Balance->get_user_balance_msg();

        $this->assign([
            'data' => $data,
            'balance'=>$balance_data
        ]);
        return $this->fetch();
    }


}