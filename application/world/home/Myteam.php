<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\Balance;
use app\world\model\User;
use app\world\model\Order;

class Myteam extends Common
{

    /**
     * @throws 我的团队
     * @author 
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 数据信息
     * @return array
     */
    public function get_list()
    {
        global $session_data;
        input('username') ? $username = input('username') : $username = ''; //搜索账号
        input('is_mortgage') ? $is_mortgage = input('is_mortgage') : $is_mortgage = ''; //搜索抵押
        input('is_balance') ? $is_balance = input('is_balance') : $is_balance = ''; //搜索结算
        input('is_brokers') ? $is_brokers = input('is_brokers') : $is_brokers = ''; //搜索经纪
        input('is_capital') ? $is_capital = input('is_capital') : $is_capital = ''; //搜索配资

        $User = new User();
        $Balance=new Balance();
        $map=array();
        if(!empty($is_balance)){
            $map['is_balance']=['>','0'];
        }
        if(!empty($is_mortgage)){
            $map['is_mortgage']=['>','0'];
        }
        if(!empty($is_brokers)){
            $map['is_brokers']=['>','0'];
        }
        if(!empty($is_capital)){
            $map['is_capital']=['>','0'];
        }
        if (!$username) {

            input('id') ? $id = input('id') : $id = session('home_user')['id']; //有接收id则查询接收id的直接下级，否则查询自身的直接下级
            input('level') ? $level = input('level') + 1 : $level = 1;      //有接收levle则当前level+1，否则levle=1
            $map['pid']=$id;
            $list = collection($User->get_one2($map))->toArray();

            if(!empty($list)){
                $list=$Balance->get_balance_level($list);
            }
//            echo json_encode($list);die;
            if (session('home_user')['group_id'] == 2) { //如果是股东，则查询所有用户
                foreach ($list as $key => $value) {
                    $list[$key]['level'] = $level;
                    $list[$key]['team_sum'] = $User->team_sum($value['id']); // 1.06s
                    $list[$key]['sum_level'] = $User->sum_level($value['id']);//0.764s
                    $list[$key]['sum_money'] = $User->sum_money($value['id']);//0.733s
                    $info = $User->get_myteam_mopney($value['id']);//2.09s
                    $list[$key]['myteam_sum_money'] = $info['money1'];
                    $list[$key]['myteam_sum_zc_money'] = $info['money2'];
                }

            } else {  //不是股东，则不显示经纪商等级大于自身的用户
                foreach ($list as $key => $value) {
                    if ($value['is_brokers'] <= session('home_user')['is_brokers']) {
                        $list[$key]['level'] = $level;
                        $list[$key]['team_sum'] = $User->team_sum($value['id']);
                        $list[$key]['sum_level'] = $User->sum_level($value['id']);
                        $list[$key]['sum_money'] = $User->sum_money($value['id']);
                        $info = $User->get_myteam_mopney($value['id']);
                        $list[$key]['myteam_sum_money'] = $info['money1'];
                        $list[$key]['myteam_sum_zc_money'] = $info['money2'];
                    }else{
                        unset($list[$key]);
                    }
                }
            }

        } else {

            $data = $User->get_select(['parent' => session('home_user')['id']]); //查询自身所有下级



            if (session('home_user')['group_id'] == 2) { //如果是股东，则显示所有用户
                foreach ($data as $k => $v) {
                    if ($v['username'] == $username) {
                        $list[$k] = $v;
                        $list[$k]['team_sum'] = $User->team_sum($v['id']);
                        $list[$k]['sum_level'] = $User->sum_level($v['id']);
                        $list[$k]['sum_money'] = $User->sum_money($v['id']);
                        $info = $User->get_myteam_mopney($v['id']);
                        $list[$k]['myteam_sum_money'] = $info['money1'];
                        $list[$k]['myteam_sum_zc_money'] = $info['money2'];
                        $list[$k]['is_balance']=$v['is_balance'];
                        if($v['balance_level']!=0){
                            $list[$k]['balance_level']=$Balance->get_balance_level($v['balance_level']);
                        }

                    }
                }

            } else { //不是股东，则不显示经纪商等级大于自身的用户

                foreach ($data as $k => $v) {
                    if ($v['username'] == $username) {
                        $a = get_parent_id($data, $v['id']);
                        $b = true;
                        foreach ($a as $vp) {   //查询该用户上级是否有经纪商等级大于自身经纪商等级
                            if ($vp['is_brokers'] > session('home_user')['is_brokers']) {
                                $b = false;
                            }
                        }
                        if ($b) {
                            $list[$k] = $v;
                            $list[$k]['team_sum'] = $User->team_sum($v['id']);
                            $list[$k]['sum_level'] = $User->sum_level($v['id']);
                            $list[$k]['sum_money'] = $User->sum_money($v['id']);
                            $info = $User->get_myteam_mopney($v['id']);

                            if(!empty($info)&&$info[0]['balance_level']!=0){

                                $info=$Balance->get_balance_level($info[0]);
                            }


                            $list[$k]['myteam_sum_money'] = $info['money1'];
                            $list[$k]['myteam_sum_zc_money'] = $info['money2'];
                            $list[$k]['is_balance']=$info[0]['is_balance'];
                            $list[$k]['balance_level']=$info[0]['balance_level'];
                        }
                    }
                }
            }
            if (@$list) {
                $list = array_merge($list);
            } else {
                return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '该用户不存在' : 'The user does not exist', 'content' => ''];
            }
        }
        $list = array_merge($list);

        return ['code' => 1, 'msg' => '', 'content' => @$list];
    }


    /**
     * 记录列表
     * @return array
     */
    public function record_list()
    {
        $username = input('username');
        if (@!$username) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $user = User::get(['username' => $username]);
        if (!$user) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $this->assign([
            'user' => $user
        ]);
        return $this->fetch();
    }


    /**
     * 資產买卖记录
     * @return array
     */
    public function orders()
    {
        $username = input('username');
        if (@!$username) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $user = User::get(['username' => $username]);
        if (!$user) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $Order = new Order();
        input('type') ? $type = input('type') : $type = 1;
        $data = $Order->get_list(['world_order.user_id' => $user->id, 'world_order.type' => $type], '', 15);
        $this->assign([
            'data' => $data,
            'type' => $type
        ]);
        return $this->fetch();
    }


    /**
     * 是否抵押商
     * @author 
     */
    public function is_mortgage()
    {
        if ($this->request->post()) {
            if (session('home_user')['group_id'] != 2) return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '權限不足' : 'Lack of authority'];
            $user = User::get(input('id'));
            $user->is_mortgage = input('is_mortgage');
            $user->save();
            return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation'];
        }

    }


//    /**
//     * 是否经纪商
//     * @author 
//     */
//    public function is_brokers()
//    {
//        if ($this->request->post()) {
//            $user = User::get(input('id'));
//            $user->brokers = input('is_brokers');
//            $user->save();
//            return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation'];
//        }
//
//    }
    /**
     * 是否结算商
     */
    public function is_balance(){
        if ($this->request->post()) {
            if (session('home_user')['group_id'] != 2) return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '權限不足' : 'Lack of authority'];
            $user = User::get(input('id'));
            $user->is_balance = input('is_balance');
            $balance_level=input('balance_level');
            $Balance=new Balance();
            if(input('is_balance')==1){//成为结算商
//                $father_balance=$Balance->get_father_balance($user);
//
//                if($father_balance['balance_level']!=0&&$father_balance['balance_level']<$balance_level) return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作失敗' : 'Failed operation'];//如果上级有结算商,判断上级结算商等级是否大于申请等级
                $user->balance_level=$balance_level;
                $user->is_balance=1;
            }else{//取消结算商资格
                $user->balance_level=0;
                $user->is_balance=0;

            }

            $user->save();
            return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation'];
        }
    }


    /**
     * 是否配资商
     * @author 
     */
    public function is_capital()
    {
        if ($this->request->post()) {
            if (session('home_user')['group_id'] != 2) return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '權限不足' : 'Lack of authority'];
            $user = User::get(input('id'));
            $user->is_capital = input('is_capital');
            $user->save();
            return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation'];
        }

    }

    /**
     * 是否经纪商
     */
    public function  is_brokers(){
        if ($this->request->post()) {
            if (session('home_user')['group_id'] != 2) return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '權限不足' : 'Lack of authority'];
            $user = User::get(input('id'));
            $data=request()->only(['is_brokers']);
            $result=$this->validate($data,[
                'is_brokers|经纪商等级' => 'require|number|in:0,1,2,3'
            ]);
            if(true !== $result){
                return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters'];
            }
            $user->is_brokers = input('is_brokers');
            $user->save();
            return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation'];
        }
    }


}