<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\User;
use app\world\model\Mortgage as MortgageModel;
use app\world\model\Order;
use app\world\model\Configs;
use app\world\model\NewsUser;

class Mortgage extends Common
{


    /**
     * 抵押
     * @author
     */
    public function main()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        $data['money'] = number_format($user_data->money1, 2); //可出借额度
        $MortgageModel = new MortgageModel();
        $data['mortgage_money'] = number_format($MortgageModel->where(['mortgage_id' => $id, 'status' => ['in', '1,2,3']])->sum('money'), 2); //已出借额度
        $data['mortgage_profit'] = number_format($MortgageModel->get_mortgage_profit($id), 2); //累计收益
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     * 抵押设置
     * @author
     */
    public function index()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        //if ($user_data->is_mortgage != 1) $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        if ($this->request->post()) {
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = input();
            if ($user_data->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            if (!is_numeric($data['mortgage']) || $data['mortgage'] < 0) $this->error(@$_SESSION['is_language'] ? '参数错误' : 'Missing parameters');
            $user_data->mortgage = $data['mortgage'];
            $user_data->save();
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('index/index'), '', 1);
        }
        $this->assign([
            'user' => $user_data,
        ]);
        return $this->fetch();
    }

    /**
     * 抵押記錄
     * @author
     */
    public function record()
    {
        $id = session('home_user')['id'];
        $Mortgage = new MortgageModel();
        $list = $Mortgage->get_all(['mortgage_id' => $id], 'create_time desc');
        if ($this->request->post()) {
            $data = input();
            $mortgage = $Mortgage->get_all(['mortgage.id' => $data['id']]);
            if ($data['status'] == 5) { //审核拒绝

                if ($mortgage[0]->status != 0) {
                    return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', 'content' => ''];
                }
                $mortgage[0]->status = $data['status'];
                $mortgage[0]->save();

                $order = Order::get($mortgage[0]->order_id);
                $order->surplus_sum += $mortgage[0]->sum;
                $order->save();

                $NewsUser = new NewsUser();
                $NewsUser->title = '資產抵押。Asset mortgage';
                $NewsUser->content = '您的抵押審核拒絕。Your mortgage review refuses';
                $NewsUser->user_id = $mortgage[0]->user_id;
                $NewsUser->save();
                return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation', 'content' => ''];
            }


            if ($data['status'] == 1) { //审核通过
                if ($mortgage[0]->status != 0) {
                    return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', 'content' => ''];
                }
                if ($mortgage[0]->mortgage_money1 < $mortgage[0]->money) return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low', 'content' => ''];
                $mortgage[0]->status = $data['status'];
                $mortgage[0]->time = time();
                $mortgage[0]->save();


                $user = User::get($mortgage[0]->user_id);
                $user->money1 += $mortgage[0]->money;
                $user->save();

                $mortgage_user = User::get($mortgage[0]->mortgage_id);
                $mortgage_user->money1 -= $mortgage[0]->money;
                $mortgage_user->save();

                $chang = '資產抵押給用戶‘'.$mortgage_user->username.'’通過 Mortgage through';
                User::money_change($user->id, 1, $mortgage[0]->money, $chang);


                $chang = '通過用户‘'.$user->username.'’的資產抵押 Mortgage through';
                User::money_change($mortgage_user->id, 0, $mortgage[0]->money, $chang);

                $NewsUser = new NewsUser();
                $NewsUser->title = '資產抵押。Asset mortgage';
                $NewsUser->content = '您的抵押審核通過。Your mortgage audit passed';
                $NewsUser->user_id = $mortgage[0]->user_id;
                $NewsUser->save();
                return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation', 'content' => ''];
            }


            if ($data['status'] == 3) { //典当
                if ($mortgage[0]->status != 1) {
                    return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', 'content' => ''];
                }
                $time1 = floor((time() - $mortgage[0]->time) / 86400); //交易天数
                $con = new Configs();
                $con_data = $con->get_find('overdue');
                $time2 = $con_data->overdue + $mortgage[0]->day;   //最大逾期时间


                if ($time1 < $time2) return ['code' => 0, 'msg' => @$_SESSION['is_language'] ? '未達到最大逾期時間' : 'Not reaching the maximum overdue time', 'content' => ''];

                $mortgage[0]->status = $data['status'];
                $mortgage[0]->time_end = time();
                $mortgage[0]->save();


                $NewsUser = new NewsUser();
                $NewsUser->title = '資產抵押。Asset mortgage';
                $NewsUser->content = '您的資產因逾期已典當。Your assets are pawned for overdue';
                $NewsUser->user_id = $mortgage[0]->user_id;
                $NewsUser->save();


                $Order = new Order();
                $Order->add_order([
                    'type' => 3,
                    'pid' => $mortgage[0]->order_pid,
                    'transfer_id' => $mortgage[0]->user_id,
                    'user_id' => $mortgage[0]->mortgage_id,
                    'goods_id' => $mortgage[0]->goods_id,
                    'money' => $mortgage[0]->money,
                    'sum' => $mortgage[0]->sum,
                    'actual_sum' => $mortgage[0]->sum,
                    'surplus_sum' => $mortgage[0]->sum,
                    'status' => 1,
                ]);
                return ['code' => 1, 'msg' => @$_SESSION['is_language'] ? '操作成功' : 'Successful operation', 'content' => ''];
            }
        }

        foreach ($list as &$value) {
            $ruls = $Mortgage->mortgage_ruls($value->id);
            $value->past_time = $ruls['past_time'];
            $value->redeem_money = $ruls['redeem_money'];
        }

        $this->assign([
            'list' => $list,
        ]);

        return $this->fetch();
    }


}