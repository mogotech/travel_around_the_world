<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\model\NewsUser as NewsUserModel;

class NewsUser extends Common
{
    /**
     * 初始化
     * @author 
     */
    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new NewsUserModel();
    }

    /**
     * 模型
     * @author 
     */
    private $model;

    /**
     * 消息列表
     * @author 
     */
    public function index()
    {
        $user_id = session('home_user')['id'];;
        $data = $this->model->get_select(['user_id' => $user_id], 'is_see,id desc', 15);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * 消息详情
     * @author 
     */
    public function main()
    {
        $id = input('id');
        if (!isset($id)) return $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $data = $this->model->get_find(['id' => $id]);

        NewsUserModel::where('id', $id)->update(['is_see' => '2']);
        if ($data->type == 2) {
            $this->redirect($data->link);
        }
        $this->assign('data', $data);
        return $this->fetch();
    }


}