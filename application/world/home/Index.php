<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;


use app\world\home\Common;
use app\world\model\Balance;
use app\world\model\News;
use app\world\model\NewsUser;
use app\world\model\Notice;
use app\world\model\User;
use app\world\model\Order;
use app\world\model\Mix;
use app\world\model\MixOut;
use app\world\model\Mortgage;
use app\world\model\Apply;
class Index extends Common
{

    /**
     * 个人中心
     * @author
     */
    public function index()
    {
        $NewsUser = new NewsUser();
        $User = new User();
        $Order = new Order();
        $Mix = new Mix();
        $MixOut = new MixOut();
        $id = session('home_user')['id'];
        $user_data = $User->get_find(['id' => $id])->toArray();

        $user_data['sum_money'] = $User->sum_money($user_data['id']);       //总资产
        $user_data['level'] = $User->sum_level($user_data['id']);           //星级
        $news_count = count($NewsUser->where(['user_id' => $id, 'is_see' => 1])->select()); //未读消息条数
        $order_count = count($Order->get_select(['pid' => $id, 'status' => 0, 'type' => ['in', [1, 4, 2, 5]]])); //交易大厅待处理条数
        $mix_count = count($Mix->get_select(['pid' => $id, 'status' => 0]));    //兑入申请提示
        $pu_mix_count = count($Mix->get_select(['user_id' => $id, 'is_see' => 0])); //兑入记录提示
        $mixout_count = count($MixOut->get_select(['pid' => $id, 'status' => 0]));  //提現申請提示
        $mixout_su_count = count($MixOut->get_select(['user_id' => $id, 'status' => 1, 'is_see' => 0]));//兑出记录提示
        $capital_count = count($Order->get_select(['transfer_id' => $id, 'status' => 4, 'type' => 4]));//配资提示
        $capital_user_count = count($Order->get_select(['user_id' => $id, 'is_prompt' => 0]));  //融資記錄提示
        $capital_user_count += count($Order->get_select(['user_id' => $id, 'bond_prompt' => 1]));//融資記錄补仓线提示
        $Mortgage = new Mortgage();
        $mortgage_count = count($Mortgage->get_all(['mortgage_id' => $id, 'mortgage.status' => 0]));    //抵押提示
        $apply_count = Apply::where(['pid'=>$id,'is_see'=>0])->count();

        if ($_SESSION['is_language']) {
            $brokers = array(1 => '區域經紀商', 2 => '大區經紀商', 3 => '國際經紀商');
        } else {
            $brokers = array(1 => 'Regional brokers', 2 => 'Large district brokers', 3 => 'International brokers');
        }

        $Balance=new Balance();
        $user_data=$Balance->get_balance_level($user_data);
        //获取公告信息 全体公告   或者 股东公告
        $Notice=new Notice();
        $notice=$Notice->get_notice();
//        echo json_encode($notice);die;
        $this->assign([
            'user_data' => $user_data,
            'news_count' => $news_count,
            'order_count' => $order_count,
            'mix_count' => $mix_count,
            'mixout_count' => $mixout_count,
            'mortgage_count' => $mortgage_count,
            'mixout_su_count' => $mixout_su_count,
            'pu_mix_count' => $pu_mix_count,
            'capital_count' => $capital_count,
            'capital_user_count' => $capital_user_count,
            'brokers' => $brokers,
            'apply_count' => $apply_count,
            'news'=>$notice
        ]);

        return $this->fetch();
    }


    public function language()
    {
        if ($this->request->post()) {
            $_SESSION['is_language'] = input('language');
            return ['code' => 1, 'msg' => '', 'content' => ''];
        }
    }
    //股东设置最低兑出金额
    public function set_min_mix_out(){
        $User = new User();
        $id = session('home_user')['id'];
        $user_data = $User->get_find(['id' => $id])->toArray();

        if ($this->request->post()) {
            $data=request()->only(['min_mix_out','max_mix_out']);

            $min_mix_out=$data['min_mix_out'];
            $user = $User->get_find(['id' => session('home_user')['id']]);

            if($user_data['group_id']!=2){
                return ['code'=>0,'msg'=>  @$_SESSION['is_language'] ? '沒有權限' : 'No permission','content'=>''];
            }
            $user->min_mix_out=$min_mix_out;
            $user->max_mix_out=$data['max_mix_out'];
            $user ->save();

            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign(
            'user',$user_data
        );
        return $this->fetch();
    }
    public function news_main($id=''){
        $Notice=new Notice();
        $notice=$Notice->get_find(['id'=>$id]);

        $this->assign([
            'data'=>$notice
            ]
        );
        return $this->fetch();
    }
}