<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\User;
use think\Request;

class Brokers extends Common
{

    /**
     * 经纪商分成设置
     */
    public function index()
    {
        $id = session('home_user')['id'];
        $user = User::find($id);

        if ($user->group_id != 2) $this->error(@$_SESSION['is_language'] ? '沒有權限' : 'No permission');
        if ($this->request->post()) {
            $User = new User();
            //得到登录密码类型
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = Request::instance()->only(['brokers', 'brokers1', 'brokers2', 'brokers3', 'password']);
            if ($user->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            $validate = $this->validate($data, 'Brokers.update');
            if ($validate !== true) $this->error($validate);

            if ($data['brokers2'] < $data['brokers1']) $this->error(@$_SESSION['is_language'] ? '區域經紀商傭金比例不能小於大區經濟商' : 'The commission ratio of regional brokers should not be less than that of large districts', url('brokers/index'), '', 1);
            if ($data['brokers3'] < $data['brokers2']) $this->error(@$_SESSION['is_language'] ? '國際經紀商傭金比例不能小於區域經紀商' : 'The commission ratio of international brokers should not be less than regional brokers', url('brokers/index'), '', 1);

            if (User::where('id', $id)->update($data)) {
                $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('brokers/index'), '', 1);
            } else {
                $this->error(@$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', url('brokers/index'), '', 1);
            }
        }
        $this->assign([
            'user' => $user
        ]);
        return $this->fetch();
    }

}