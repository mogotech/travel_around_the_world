<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\User;
use app\world\model\Wallet as WalletModel;
use app\world\model\WalletRuls;
use app\world\model\Configs;

class Wallet extends Common
{


    /**
     * 电子钱包
     * @author 
     */
    public function main()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        $data['sum_money'] = $user_data->money5;
        $beginYesterday = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
        $endYesterday = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
        $WalletRuls = new WalletRuls();
        $Yesterday = $WalletRuls->where(['user_id' => $id, 'create_time' => ['BETWEEN', [$beginYesterday, $endYesterday]]])->find();
        $data['yesterday_money'] = $Yesterday ? $Yesterday->add_money : 0;
        $Cumulative = $WalletRuls->where(['user_id' => $id])->sum('add_money');
        $data['cumulative_money'] = $Cumulative ? $Cumulative : 0;
        $Configs = new Configs();
        $con = $Configs->get_find();
        $data['electronics_wallet'] = $con->electronics_wallet;

        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }

    /**
     * 电子钱包-余额转钱包
     * @author 
     */
    public function index()
    {
        $User = new User();
        $id = session('home_user')['id'];
        $user_data = $User->get_find(['id' => $id]);
        if ($this->request->post()) {
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = input();
            if ($user_data->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            if (!is_numeric($data['money']) || $data['money'] < 0) $this->error(@$_SESSION['is_language'] ? '参数错误' : 'Missing parameters');
            if ($data['money'] > $user_data->money1) $this->error(@$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low');
            $user_data->money1 -= $data['money'];
            $user_data->money5 += $data['money'];
            $user_data->save();
            $WalletModel = new WalletModel();
            $info['user_id'] = $id;
            $info['money'] = $data['money'];
            $WalletModel->add($info);
            User::money_change($user_data->id, 0, $data['money'], '轉入電子錢包 Electronic wallet');
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('index/index'), '', 1);
        }
        $this->assign([
            'user_data' => $user_data,
        ]);
        return $this->fetch();
    }


    /**
     * 电子钱包-钱包转余额
     * @author 
     */
    public function transformation()
    {
        $User = new User();
        $id = session('home_user')['id'];
        $user_data = $User->get_find(['id' => $id]);
        if ($this->request->post()) {
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = input();
            if ($user_data->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            if (!is_numeric($data['money']) || $data['money'] < 0) $this->error(@$_SESSION['is_language'] ? '参数错误' : 'Missing parameters');
            if ($data['money'] > $user_data->money5) $this->error(@$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low');
            $user_data->money1 += $data['money'];
            $user_data->money5 -= $data['money'];
            $user_data->save();
            $WalletModel = new WalletModel();
            $info['user_id'] = $id;
            $info['money'] = $data['money'];
            $info['type'] = 2;
            $WalletModel->add($info);
            User::money_change($user_data->id, 1, $data['money'], '轉出電子錢包 Electronic wallet');
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('index/index'), '', 1);
        }
        $this->assign([
            'user_data' => $user_data,
        ]);
        return $this->fetch();
    }


    /**
     * 电子钱包-余额转钱包-记录
     * @author 
     */
    public function record()
    {
        $map['type'] = 1;
        $map['user_id'] = session('home_user')['id'];
        $WalletModel = new WalletModel();
        $data = $WalletModel->get_all($map);
        foreach ($data as &$v) {
            $v['time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     * 电子钱包-钱包转余额-记录
     * @author 
     */
    public function transformation_record()
    {
        $map['type'] = 2;
        $map['user_id'] = session('home_user')['id'];
        $WalletModel = new WalletModel();
        $data = $WalletModel->get_all($map);
        foreach ($data as &$v) {
            $v['time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     * 电子钱包-增值记录
     * @author 
     */
    public function record_ruls()
    {
        $map['user_id'] = session('home_user')['id'];
        $WalletRuls = new WalletRuls();
        $data = $WalletRuls->get_all($map);
        foreach ($data as &$v) {
            $v['time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


}