<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;


use app\index\controller\Home;
use think\Db;


class Base extends Home
{

    /**
     * 初始化方法
     */
    protected function _initialize()
    {
        parent::_initialize();
        session_start();
        @$_SESSION['is_language'] ? $this->assign('is_language', $_SESSION['is_language']) : '';
    }


    /**
     * 选择语言
     */
    public function is_language()
    {
        if ($this->request->post()) {
            $_SESSION['is_language'] = input('language');
            return ['code' => 1, 'msg' => '', 'content' => ''];
        }
    }


}