<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\index\controller\Home;
use app\world\model\Ceshi;
use app\world\model\User;
use app\world\model\Goods;
use app\world\model\GoodsPrice;
use app\world\model\Configs;
use app\world\model\GoodsRuls;
use think\Db;
use app\world\home\Base;
use app\world\model\WalletRuls;
use app\world\model\Order;
class Login extends Base
{

    /**
     * 初始化
     * @author 
     */
    protected function _initialize()
    {
        parent::_initialize();
        isset($_SESSION['dolphin_admin_']['home_user']) ? $this->redirect('index/index') : '';
        $this->model = new User();
    }

    /**
     * 模型
     * @author 
     */
    private $model;


    /**
     * 选择语言
     * @author 
     */
    public function is_language()
    {
        if (is_numeric(input('is_language'))) {
            $_SESSION['is_language'] = input('is_language');
            return $this->redirect('index');
        }
        return $this->fetch();
    }


    /**
     * 登陆
     * @author 
     */
    public function index()
    {
        if (!isset($_SESSION['is_language'])) {
            return $this->redirect('is_language');
        }
        if ($this->request->post()) {
            $data = input();
            $b = $this->model->login($data);
            if ($b === true) {
                $this->success($_SESSION['is_language'] ? '登陆成功' : 'Login Successful', url('goods/index'), '', 1);
            } elseif (isset($b['code']) && @$b['code'] == 0) {
                $this->error($b['msg'], 'illegal');
            } else {
                $this->error($b);
            }
        }
        return $this->fetch();
    }
    /**
     * 注册
     * @author 
     */
    public function register()
    {
        if (!isset($_SESSION['is_language'])) {
            return $this->redirect('is_language');
        }
        if ($this->request->post()) {
            $data = input();
            if ($data['password1'] != $data['password1_new']) $this->error($_SESSION['is_language'] ? '兩次密碼不一致' : 'The two password is inconsistent');
            $validate = $this->validate($data, 'User.home_add');
            if ($validate !== true) $this->error($validate);
            $info['username'] = $data['username'];
            $info['password1'] = $data['password1'];
            $info['password1_new'] = $data['password1_new'];
            $info['code'] = $data['code'];
            $b = $this->model->add_user($info, 0);
            if ($b === true) {
                $this->success($_SESSION['is_language'] ? '注冊成功' : 'login was successful', url('index'), '', 1);
            } else {
                $this->error($b);
            }
        }
        return $this->fetch();
    }


    /**
     * 非法操作
     * @author 
     */
    public function illegal()
    {
        return $this->fetch();
    }


//测试刷新資產价格 新增自动平仓
    public function ceshi()
    {

        if (input('cc') != '93a90b0f9007327a7f65c9ec1d93e9bd') {
            echo 11;
            die;
        }
        $model = new Goods();
        $model->update_money();
        echo '刷新資產价格';
        $model->apitalc();
    }

    //昨日资产卖出数量
    public function ceshi2()
    {
        if (input('cc') != '93a90b0f9007327a7f65c9ec1d93e9bd') {
            die;
        }
        $model = new Goods();
        $data = $model->get_goods();
        foreach ($data as $v) {
            if (is_numeric($v->shop_max) && $v->shop_max > 0) {
                $FloatLength = getFloatLength($v->shop_min);
                $jishu_rand = pow(10, $FloatLength);
                $shop_min = $v->shop_min * $jishu_rand;
                $shop_max = $v->shop_max * $jishu_rand;
                $rand = rand($shop_min, $shop_max) / $jishu_rand;
                $info = $model->find($v->id);
                $info->shop_money = $rand;
                $info->save();
            } else {
                $info = $model->find($v->id);
                $info->shop_money = 0;
                $info->save();
            }
        }
    }


    //电子钱包
    public function ceshi3()
    {
        if (input('cc') != '93a90b0f9007327a7f65c9ec1d93e9bd') {
            die;
        }

        $User = new User();
        $Con = new Configs();
        $b = $Con->get_find(['electronics_wallet']);
        $list = $User->get_select(['status' => 1]);
        foreach ($list as $value) {
            if ($value->money5 > 0) {
                $user = $User::get($value->id);
                $user->money5 += $user->money5 * $b->electronics_wallet;
                $user->save();
                $WalletRuls = new WalletRuls();
                $data['user_id'] = $value->id;
                $data['money'] = $value->money5;
                $data['add_money'] = $user->money5 * $b->electronics_wallet;
                $data['proportion'] = $b->electronics_wallet;
                $WalletRuls->add($data);
            }
        }

    }


    //平台昨日交易金额
    public function ceshi4()
    {
        if (input('cc') != '93a90b0f9007327a7f65c9ec1d93e9bd') {
            die;
        }
        $model = new Configs();
        $data = $model->get_find();
        if (is_numeric($data->shop_max) && $data->shop_max > 0) {
            $FloatLength = getFloatLength($data->shop_min);
            $jishu_rand = pow(10, $FloatLength);
            $shop_min = $data->shop_min * $jishu_rand;
            $shop_max = $data->shop_max * $jishu_rand;
            $rand = rand($shop_min, $shop_max) / $jishu_rand;
            $data->shop_money = $rand;
            $data->save();
        }
    }


    public function ceshi5()
    {
        $model = new Goods();
        $model->apitalc();
    }







//    public function aa()
//    {
//        ini_set('max_execution_time', '0');
//        $Configs = new Configs();
//        $con = $Configs->get_find();
//        $start_time = $con->start_time;   //开市时间
//        $end_time = $con->end_time;       //闭市时间
//
//        //die;
//        $time = strtotime('2014-08-14 00:00:00');
//        $time1 = time();
//        //$time1 = strtotime('2017-09-30 00:00:00');
//        //$Goods = Goods::get(input('id'));
//        //$time = $Goods->create_time;
//
//        $count = ($time1 - $time) / 3600; //循环次数
//
//        $Goods = new Goods;
//        $Goods->user_id = 0;
//        $Goods->name = '4-8-比克';
//        $Goods->int_money = '9.25';
//        $Goods->section = '1.5';
//        $Goods->create_time = $time;
//        $Goods->save();
//        $data = [
//            ['goods_id' => $Goods->id, 'money' => $Goods->int_money, 'times' => $time],
//            ['goods_id' => $Goods->id, 'money' => 100, 'times' => strtotime('2014-10-26 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 500, 'times' => strtotime('2015-02-02 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 800, 'times' => strtotime('2015-04-16 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 1000, 'times' => strtotime('2015-05-29 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 800, 'times' => strtotime('2015-07-06 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 1200, 'times' => strtotime('2015-12-05 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 2000, 'times' => strtotime('2016-04-08 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 5000, 'times' => strtotime('2016-12-15 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 7000, 'times' => strtotime('2017-05-19 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 9000, 'times' => strtotime('2017-08-04 00:00:00')],
//            ['goods_id' => $Goods->id, 'money' => 11500, 'times' => strtotime('2017-12-04 00:00:00')],
//        ];
//
//        $GoodsRuls = new GoodsRuls;
//        $GoodsRuls->saveAll($data);
//
//
//        $sql = "insert into dp_world_goods_price (goods_id,money,create_time) values";
//        for ($i = 0; $i < (int)$count; $i++) {
//            $goods_id = $Goods->id;
//            if (date('H:i:s', $time) >= $start_time && date('H:i:s', $time) <= $end_time) {
//                $data = $this->bb($goods_id, $time);
//                $sql .= "(" . $data['price']['goods_id'] . "," . $data['price']['money'] . "," . $data['price']['create_time'] . "),";
//            }
//            $time += 3600;
//        }
//        $sql = substr($sql, 0, strlen($sql) - 1);
//        Db::execute($sql);
//        $Goods->money = $data['goods']['money'];
//        $Goods->new_money = $data['goods']['new_money'];
//        $Goods->time = $data['goods']['time'];
//        $Goods->save();
//    }
//
//
//    public function bb($goods_id, $time)
//    {
//        $Goods = new Goods;
//        $data = $Goods->find($goods_id)->toArray();
//
//        if ($data) {   //如果有需要刷新价格的資產
//            //查询上一次的价格节点
//            $ruls_sql = "select * from dp_world_goods_ruls
//                                where
//                                    goods_id = " . $goods_id . " and  times <= " . $time . "
//                                order
//                                    BY times desc
//                                limit 1";
//
//            $info = Db::query($ruls_sql);
//            //是否有设置价格节点
//            $ruls_sql = "select * from dp_world_goods_ruls
//                                where
//                                    goods_id = " . $goods_id . " and  times > " . $time . "
//                                order
//                                    BY times
//                                limit 1";
//            $ruls_data = Db::query($ruls_sql);
//
//
//
//            if ($ruls_data) {//有设置价格节点
//                $a_time = $ruls_data[0]['times'] - $info[0]['times'];
//                $a_money = $ruls_data[0]['money'] - $info[0]['money'];
//                $a = $a_money / $a_time;
//                $b = $info[0]['money'] - $a * $info[0]['times'];
//                $new_money = $time * $a + $b;     //新的价格标准
//            } else {
//                $new_money = $info[0]['money']; //价格标准
//            }
//
//            $money = mt_rand($new_money - $data['section'], $new_money + $data['section']); //当前价格
//            $f = mt_rand(1, 100); //随机小数
//            $money = $money + ($f / 100);
//            $money < 0 ? $money = 0 : ''; //价格最低为0
//
//            $datas['price']['goods_id'] = $goods_id;
//            $datas['price']['money'] = $money;
//            $datas['price']['create_time'] = $time;
//
//            $datas['goods']['money'] = $money;
//            $datas['goods']['new_money'] = $new_money;
//            $datas['goods']['time'] = $time;
//            return $datas;
//        }
//
//    }


//        public function cc()
//        {
//
//
//
//
//
//            $data=[
//                ['create_time'=>1296518400,'money'=>'116.38','goods_id'=>1],
//                ['create_time'=>1297296000,'money'=>'131.63','goods_id'=>1],   2
//                ['create_time'=>1298160000,'money'=>'112.95','goods_id'=>1],
//                ['create_time'=>1298851200,'money'=>'121.16','goods_id'=>1],
//
//
//
//
//                ['create_time'=>1298937600,'money'=>'119.72','goods_id'=>1],
//                ['create_time'=>1299283200,'money'=>'138.52','goods_id'=>1],
//                ['create_time'=>1299715200,'money'=>'117','goods_id'=>1],       3
//                ['create_time'=>1301529600,'money'=>'137.80','goods_id'=>1],
//
//
//
//
//
//                ['create_time'=>1301616000,'money'=>'138.45','goods_id'=>1],
//                ['create_time'=>1301685412,'money'=>'156.04','goods_id'=>1],
//                ['create_time'=>1301725412,'money'=>'136.02','goods_id'=>1],     4
//                ['create_time'=>1304035200,'money'=>'148.52','goods_id'=>1],
//
//
//
//
//                ['create_time'=>1304208000,'money'=>'149.93','goods_id'=>1],
//                ['create_time'=>1304307660,'money'=>'150.50','goods_id'=>1],
//                ['create_time'=>1305765439,'money'=>'124.66','goods_id'=>1],    5
//                ['create_time'=>1306800000,'money'=>'135.71','goods_id'=>1],
//
//
//
//                ['create_time'=>1306886400,'money'=>'135.00','goods_id'=>1],
//                ['create_time'=>1307159563,'money'=>'140.25','goods_id'=>1],
//                ['create_time'=>1308423942,'money'=>'114.14','goods_id'=>1],    6
//                ['create_time'=>1309392000,'money'=>'140.13','goods_id'=>1],
//
//
//                ['create_time'=>1309478400,'money'=>'139.44','goods_id'=>1],
//                ['create_time'=>1310659852,'money'=>'165.96','goods_id'=>1],
//                ['create_time'=>1310959852,'money'=>'137.70','goods_id'=>1],    7
//                ['create_time'=>1311897600,'money'=>'157.07','goods_id'=>1],
//
//
//
//                ['create_time'=>1312156800,'money'=>'161.70','goods_id'=>1],
//                ['create_time'=>1310659852,'money'=>'161.90','goods_id'=>1],
//                ['create_time'=>1310959852,'money'=>'125.11','goods_id'=>1],    8
//                ['create_time'=>1314748800,'money'=>'145.78','goods_id'=>1],
//
//
//
//
//
//                ['create_time'=>1314835200,'money'=>'145.03','goods_id'=>1],
//                ['create_time'=>1315512349,'money'=>'150.46','goods_id'=>1],
//                ['create_time'=>1316952215,'money'=>'100.95','goods_id'=>1],    9
//                ['create_time'=>1317340800,'money'=>'106.91','goods_id'=>1],
//
//
//
//
//                ['create_time'=>1312156800,'money'=>'104.71','goods_id'=>1],
//                ['create_time'=>1310659852,'money'=>'147.68','goods_id'=>1],
//                ['create_time'=>1310959852,'money'=>'102.00','goods_id'=>1],    10
//                ['create_time'=>1314748800,'money'=>'145.78','goods_id'=>1],
//
//
//
//            ];
//
//
//
//
//
//
//
//
//        }


//    public function dd()
//    {
//        Ceshi::get_data();
//    }
//

}