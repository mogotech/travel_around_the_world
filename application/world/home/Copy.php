<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;


use app\world\home\Common;


class Index111 extends Common
{

    /**
     * 运单列表(员工)
     * @author
     */
    public function order()
    {
        if (!isset($_SESSION['dolphin_admin_']['home_user']['group_id'])) $this->redirect('user_order');
        $store = StoreModel::where(['status' => 1])->field('id,name')->select();
        $this->assign('store', $store);
        return $this->fetch();
    }

}