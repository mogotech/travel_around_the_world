<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\model\User as UserModel;

class User extends Common
{


    /**
     * 初始化
     * @author 
     */
    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new UserModel();
    }

    /**
     * 模型
     * @author 
     */
    private $model;

    /**
     * 关联账号列表
     * @author 
     */
    public function lists()
    {
        $users = $this->model->get_select(['pid' => session('home_user')['id'], 'group_id' => 1], '', '15');
        foreach ($users as $v) {
            $v->sum_money = $this->model->sum_money($v->id);
        }
        $this->assign('users', $users);
        return $this->fetch();
    }

    /**
     * 增加/修改小号
     * @author 
     */
    public function main()
    {
        if ($this->request->post()) {
            $data = input();
            if (@$data['id']) { //编辑
                $validate = $this->validate($data, 'User.home_edit2');
                if ($validate !== true) $this->error($validate);
                if ($data['password1']) $data['password1'] = $this->model->add_password($data['password1']); else unset($data['password1']);
                $b = $this->model->edit_user($data, 1);
            } else { //新增
                $validate = $this->validate($data, 'User.home_add2');
                if ($validate !== true) $this->error($validate);
                $data['pid'] = session('home_user')['id'];
                $b = $this->model->add_user($data, 1);
            }

            if ($b) {
                $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
            } else {
                $this->error(@$_SESSION['is_language'] ? '操作失敗' : 'Operation failed');
            }
        }
        $id = input('id');
        if ($id) {
            $user = $this->model->get_find(['pid' => session('home_user')['id'], 'group_id' => 1, 'id' => $id]);
            if ($user) {
                $this->assign('user', $user);
            }
        }
        return $this->fetch();
    }


    /**
     * 修改资料
     * @author 
     */
    public function edit()
    {
        $id = session('home_user')['id'];
        $user = $this->model->get_find(['id' => $id]);
        if ($this->request->post()) {
            $data = input();
            $validate = $this->validate($data, 'User.home_edit');
            if ($validate !== true) $this->error($validate);
            if ($data['new_password']) {
                if (session('home_password') == 1) {
                    if ($user->password1 != $this->model->add_password($data['password'])) {
                        $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
                    }
                    $info['password1'] = $this->model->add_password($data['new_password']);
                } else if (session('home_password') == 2) {
                    if ($user->password2 != $this->model->add_password($data['password'])) {
                        $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
                    }
                    $info['password2'] = $this->model->add_password($data['new_password']);
                }
                unset($data['password']);
                unset($data['new_password']);
            }
            $info['id'] = $data['id'];
            $info['nickname'] = $data['nickname'];
            $this->model->edit_user($info);
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign('user', $user);
        return $this->fetch();
    }


    /**
     * 退出登陆
     * @author 
     */
    public function out_login()
    {
        unset($_SESSION['dolphin_admin_']);
        unset($_SESSION['is_language']);
        $this->redirect('login/index');
    }

}