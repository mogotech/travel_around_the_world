<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\Apply as ApplyModel;
use app\world\model\User;
use think\Db;

class Apply extends Common
{

    /**
     *申请列表
     */
    public function index()
    {
        $user_id = session('home_user')['id'];
        $data = ApplyModel::where(['type' => ['GT', 0], 'user_id' => $user_id])->order('id desc')->select();
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     *反馈列表
     */
    public function feedback()
    {
        $user_id = session('home_user')['id'];
        $data = ApplyModel::where(['type' => 0, 'user_id' => $user_id])->order('id desc')->select();
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     *添加反馈
     */
    public function feedback_add()
    {
        $user_id = session('home_user')['id'];
        $User = new User();
        $user = $User->find($user_id);
        if ($this->request->post()) {
            $data = input();
            $data['pid'] = $User->get_pid($user_id);
            $data['user_id'] = $user_id;
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            if ($user->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            $validate = $this->validate($data, 'Apply.add');
            if ($validate !== true) $this->error($validate);
            if (ApplyModel::create($data)) {
                $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('feedback'), '', 1);
            } else {
                $this->error(@$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', url('feedback'), '', 1);
            }
        }
        return $this->fetch();
    }

    /**
     *添加申请
     */
    public function add()
    {
        $user_id = session('home_user')['id'];
        $User = new User();
        $user = $User->find($user_id);

        if ($this->request->post()) {
            $data = input();
            $data['pid'] = $User->get_pid($user_id);
            $data['user_id'] = $user_id;
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            if ($user->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            $validate = $this->validate($data, 'Apply.add');
            if ($validate !== true) $this->error($validate);
            if (ApplyModel::create($data)) {
                $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('index'), '', 1);
            } else {
                $this->error(@$_SESSION['is_language'] ? '操作失敗' : 'Operation failed', url('index'), '', 1);
            }
        }
        return $this->fetch();
    }


    /**
     *申请详情
     */
    public function main()
    {
        $user_id = session('home_user')['id'];
        $id = input('id');
        $data = ApplyModel::where(['id' => $id, 'user_id' => $user_id])->find();
        if (!$data) {
            $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     *反馈详情
     */
    public function feedback_main()
    {
        $user_id = session('home_user')['id'];
        $id = input('id');
        $data = ApplyModel::where(['id' => $id, 'user_id' => $user_id])->find();
        if (!$data) {
            $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     *股东查看
     */
    public function see()
    {
        $user_id = session('home_user')['id'];
        $data = Db::view('world_apply a', true)
            ->view('world_user user', 'username', 'a.user_id=user.id')
            ->where(['a.pid' => $user_id,'type'=>['in',[0,1,2,3,4]]])
            ->order('a.id desc')
            ->select();
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


    /**
     *股东查看详情
     */
    public function see_main()
    {
        $user_id = session('home_user')['id'];
        $id = input('id');
        $data = ApplyModel::where(['id' => $id, 'pid' => $user_id])->find();
        if (!$data) {
            $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        }
        ApplyModel::where(['id' => $id])->update(['is_see' => 1]);
        if($this->request->post()){
            $data = input('reply');
            ApplyModel::update(['reply'=>$data],['id'=>$id]);
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('see'), '', 1);
        }
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }


}