<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;


use app\world\home\Common;
use app\world\model\Moneychange as MoneychangeModel;


class Moneychange extends Common
{

    /**
     * 金额变动
     * @author
     */
    public function index()
    {
        return $this->fetch();
    }


    public function ajax()
    {
        $id = session('home_user')['id'];
        $list = MoneychangeModel::where(['user_id' => $id])->order('id desc')->paginate();
        foreach ($list as &$v) {
            $v['time'] = date('Y-m-d H:i', $v['create_time']);
        }
        return $list;
    }


}