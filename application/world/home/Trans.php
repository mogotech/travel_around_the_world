<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\home\Common;
use app\world\model\User;
use app\world\model\Trans as TransModel;
use app\world\model\NewsUser;
use app\world\model\Configs;
use app\world\model\Transformation;


class Trans extends Common
{

    /**
     * 定向交易
     * @author
     */
    public function index()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        input('username') ? $username = input('username') : $username = '';
        $configs = Configs::get(1);
        if ($this->request->post()) {
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = input();
            if ($user_data->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            $p_user = $User->get_find(['username' => $data['username']]);
            if (!$p_user) $this->error(@$_SESSION['is_language'] ? '轉出用戶不存在' : 'Out of user does not exist');
            if ($p_user->id == session('home_user')['id']) $this->error(@$_SESSION['is_language'] ? '轉出用戶不能是自己' : 'Out of the user can not be their own');
            $pid1 = $User->get_pid($id);
            $pid2 = $User->get_pid($p_user->id);
            if ($pid1 != $pid2) $this->error(@$_SESSION['is_language'] ? '該用戶不符合規則' : 'The user does not conform to the rules');

            if (!is_numeric($data['money']) || $data['money'] < 0) $this->error(@$_SESSION['is_language'] ? '参数错误' : 'Missing parameters');
            if ($data['money'] > $user_data->money1) $this->error(@$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low');
            $TransModel = new TransModel();
            $TransModel->receive_id = $p_user->id;
            $TransModel->user_id = $user_data->id;
            $TransModel->money = $data['money'];
            $TransModel->save();
            $NewsUser = new NewsUser();
            $NewsUser->title = '積分互轉消息。Directional transaction message';
            $NewsUser->content = '您收到一筆轉入金額,請前往積分互轉详情查看。You have received a transfer amount. Please go to the directional transaction for details';
            $NewsUser->user_id = $p_user->id;
            $NewsUser->save();
            $user_data->money1 -= $data['money'];
            $user_data->save();
            $p_user->money1 += $data['money'];
            $p_user->save();


            $chang = '轉給用戶‘' . $p_user->username . '’積分 Orienteering';
            User::money_change($user_data->id, 0, $data['money'], $chang);
            $chang = '收到用護‘' . $user_data->username . '’積分 Orienteering';
            User::money_change($p_user->id, 1, $data['money'], $chang);


            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign([
            'user_data' => $user_data,
            'username' => $username,
            'configs' => $configs
        ]);
        return $this->fetch();
    }


    /**
     * 定向交易记录
     * @author
     */
    public function record()
    {
        if (input('username')) {
            $user = User::get(['username' => input('username')]);
            if (!$user) $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        }
        isset($user) ? $map['user_id'] = $user->id : $map['user_id'] = session('home_user')['id'];
        $TransModel = new TransModel();
        input('type') ? $type = input('type') : $type = 1;
        $data = $TransModel->get_list($map['user_id'], $type, 'dp_world_trans.id desc', 15);
        $this->assign([
            'data' => $data,
            'type' => $type,
            'user' => @$user
        ]);
        return $this->fetch();
    }


    /**
     * 余额转资产余额
     * @author
     */
    public function transformation()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        if ($user_data->group_id != 2) $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        if ($this->request->post()) {
            $pwd = session('home_password');
            $password = 'password' . $pwd;
            $data = input();
            if ($user_data->$password != $User->add_password($data['password'])) $this->error(@$_SESSION['is_language'] ? '密碼錯誤' : 'Password error');
            if (!is_numeric($data['money']) || $data['money'] < 0) $this->error(@$_SESSION['is_language'] ? '参数错误' : 'Missing parameters');
            if ($data['money'] > $user_data->money1) $this->error(@$_SESSION['is_language'] ? '餘額不足' : 'Sorry, your credit is running low');
            $Transformation = new Transformation();
            $Transformation->user_id = $user_data->id;
            $Transformation->money = $data['money'];
            $Transformation->save();
            $user_data->money1 -= $data['money'];
            $user_data->money4 += $data['money'];
            $user_data->save();
            User::money_change($user_data->id, 0, $data['money'], '積分轉資產余額 Balance of assets');
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', url('index/index'), '', 1);
        }
        $this->assign([
            'user' => $user_data,
        ]);
        return $this->fetch();
    }

    /**
     * 余额转资产余额记录
     * @author
     */
    public function transformation_record()
    {
        $id = session('home_user')['id'];
        $User = new User();
        $user_data = $User->get_find(['id' => $id]);
        if ($user_data->group_id != 2) $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
        $Transformation = new Transformation();
        $data = $Transformation->get_list($user_data->id, 'id', '15');
        $this->assign([
            'user' => $user_data,
            'data' => $data
        ]);
        return $this->fetch();
    }

}