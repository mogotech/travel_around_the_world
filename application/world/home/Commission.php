<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\model\User as UserModel;
use app\world\model\Commission as CommissionModel;


class Commission extends Common
{

    /**
     * 奖金兑换
     * @author
     */
    public function index()
    {
        $User = new UserModel();
        $user = $User->get_find(['id' => session('home_user')['id']]);
        if ($this->request->post()) {
            $money2 = input('money2');
            if (!is_numeric($money2) || $money2 < 0) $this->error(@$_SESSION['is_language'] ? '參數错误' : 'Missing parameters');
            if ($money2 > $user->money2) $this->error(@$_SESSION['is_language'] ? '獎金不足,無法兌換' : 'Sorry, your credit is running low');
            $user->money1 += $money2;
            $user->money2 -= $money2;
            $user->save();
            User::money_change($user->id, 1, $money2, '獎金兌換 Bonus Exchange');
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign([
            'user' => $user,
        ]);
        return $this->fetch();
    }


    /**
     * 奖金记录
     * @author
     */
    public function comlist()
    {
        $Commission = new CommissionModel();
        $data = $Commission->get_list(['world_commission.pid' => session('home_user')['id']], 'id desc');
        $this->assign([
            'data' => $data
        ]);
        return $this->fetch();
    }

}