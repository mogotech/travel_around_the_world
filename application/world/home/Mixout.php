<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\world\model\Balance;
use app\world\model\Configs;
use app\world\model\User as UserModel;
use app\world\model\NewsUser;
use app\world\model\MixOut as MixOutModel;

class Mixout extends Common
{

    /**
     * 兑出
     * @author
     */
    public function index()
    {
        return $this->fetch();
    }


    /**
     * 设置兑出账号
     * @author
     */
    public function setWithdraw()
    {
        $User = new UserModel();
        $user = $User->get_find(['id' => session('home_user')['id']]);
        if ($this->request->post()) {
            $data = input();
            $validate = $this->validate($data, 'User.home_edit3');
            if ($validate !== true) $this->error($validate);
            $user->allowField(['withdrawal_type', 'account', 'account_name', 'bank', 'acc_bank', 'is_acc'])->save($data);
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);

        }
        $this->assign([
            'user' => $user,
        ]);
        return $this->fetch();
    }


//    /**
//     * 兑出余额
//     * @author
//     */
//    public function withdraw()
//    {
//        $User = new UserModel();
//        $user = $User->get_find(['id' => session('home_user')['id']]);
//        $Configs = new Configs();
//        $configs_data = $Configs->get_find();
//        if ($this->request->post()) {
//            if ($user->is_acc == 0) $this->error('請設置提現賬號');return ['code'=>0,'msg'=>'請設置提現賬號','content'=>''];
//            $data = input();
//            if (!is_numeric($data['money1']) || $data['money1'] < 0) $this->error('參數不正確');
//            if ($user->password1 != $User->add_password($data['password'])) $this->error('密码错误');
//            if ($user->money1 < $data['money1']) $this->error('余额不足');
//            $MixOut = new MixOutModel();
//            $Configs = new Configs();
//            $NewsUser = new NewsUser();
//            $configs_data = $Configs->get_find();
//            $info['pid'] = $User->get_pid($user->id);
//            $info['user_id'] = $user->id;
//            $info['withdrawal_type'] = $user->withdrawal_type;
//            $info['account'] = $user->account;
//            $info['account_name'] = $user->account_name;
//            $info['bank'] = $user->bank;
//            $info['acc_bank'] = $user->acc_bank;
//            $info['mix_money'] = $data['money1'];
//            $info['exchange'] = $configs_data->exchange;
//            $info['fee_money'] = $configs_data->fee_money;
//            $info['money'] = $configs_data->exchange * $data['money1'] - $data['money1'] * $configs_data->fee_money;
//            $MixOut->add_mix_out($info);
//            $NewsUser->user_id = $user->id;
//            $NewsUser->title = '兑出消息';
//            $NewsUser->content = '您的兑出申请正在匹配交易会员,请前往兑出记录查看详情';
//            $NewsUser->save();
//            $user->money1 -= $data['money1'];
//            $user->save();
//            $this->success('操作成功');
//        }
//        $this->assign([
//            'user' => $user,
//            'configs_data' => $configs_data,
//        ]);
//        return $this->fetch();
//    }


    /**
     * 兑出余额
     * @author
     */
    public function withdraw()
    {
        $User = new UserModel();
        $user = $User->get_find(['id' => session('home_user')['id']]);
        $Configs = new Configs();
        $configs_data = $Configs->get_find();
        $Balance=new Balance();
        $father=$Balance->get_gudong_min_mix_out_set($user);
        $user['min_mix_out']=$father['min_mix_out'];
        $user['max_mix_out']=$father['max_mix_out'];
        if ($this->request->post()) {
            if ($user->is_acc == 0) return ['code' => 0, 'msg' => '請設置提現賬號', 'msg1' => 'Please set the out of account number', 'content' => ''];
            $data = input();
            if (!is_numeric($data['money1']) || $data['money1'] < 0) return ['code' => 0, 'msg' => '參數不正確', 'msg1' => 'parameter is incorrect', 'content' => ''];
            if (floor($data['money1']) != $data['money1']) return ['code' => 0, 'msg' => '提現必須是整數', 'msg1' => 'It must be an integer', 'content' => ''];
            if ($user->password1 != $User->add_password($data['password'])) return ['code' => 0, 'msg' => '密碼錯誤', 'msg1' => 'Password error', 'content' => ''];
            if ($user->money1 < $data['money1']) return ['code' => 0, 'msg' => '餘額不足', 'msg1' => 'Sorry, your credit is running low', 'content' => ''];
            $MixOut = new MixOutModel();
            $Configs = new Configs();
            $NewsUser = new NewsUser();
            $configs_data = $Configs->get_find();

            $father=$Balance->get_father_balance($user);//获取上级结算商信息

            $info['pid'] =$father['id'];//兑入的上级变成了  当前用户上级结算商 或者 股东
//            $info['pid'] = $User->get_pid($user->id);
            $info['user_id'] = $user->id;
            $info['withdrawal_type'] = $user->withdrawal_type;
            $info['account'] = $user->account;
            $info['account_name'] = $user->account_name;
            $info['bank'] = $user->bank;
            $info['acc_bank'] = $user->acc_bank;
            $info['mix_money'] = $data['money1'];
            $info['exchange'] = $configs_data->exchange;
            $info['fee_money'] = $configs_data->fee_money;
            $info['money'] = $configs_data->exchange * ($data['money1'] - $data['money1'] * $configs_data->fee_money);
            $MixOut->add_mix_out($info);
            $NewsUser->user_id = $user->id;
            $NewsUser->title = '提現消息。Exchange news';
            $NewsUser->content = '您的消息已發出，正在匹配餘額買家。Your message has been sent and matching buyer is being matched';
            $NewsUser->save();
            $user->money1 -= $data['money1'];
            $user->save();
            UserModel::money_change($user->id,0,$data['money1'],'兑出申请');
            return ['code' => 1, 'msg' => '您的消息已發出，正在匹配餘額買家', 'msg1' => 'Your message has been sent and matching buyer is being matched', 'content' => ''];
        }

        $this->assign([
            'user' => $user,
            'configs_data' => $configs_data,
        ]);
        return $this->fetch();
    }


    /**
     * 兑出申请
     * @author
     */
    public function mixout_apply_list()
    {
        $MixOut = new MixOutModel();
        if (is_numeric(input('status'))) $search['world_mix_out.status'] = input('status');
        if (input('username')) $search['world_user.username'] = input('username');
        $search['world_mix_out.pid'] = session('home_user')['id'];
        $data = $MixOut->get_list($search, 'status,id desc', 15);

        $this->assign([
            'data' => $data,
            'search' => $search,
        ]);
        return $this->fetch();
    }


    /**
     * 兑出申请详情
     * @author
     */
    public function mixout_apply_main()
    {
        $id = input('id');
        if (!$id) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        $MixOut = new MixOutModel();
        $data = $MixOut->get_find(['world_mix_out.id' => $id]);
        if ($this->request->post()) {
            if($data->status!=0)$this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
            $NewsUser = new NewsUser();
            $NewsUser->user_id = $data->user_id;
            $info = input();
            if ($info['status'] == 1) {
                $p_user = UserModel::get($data->pid);
                $p_user->money1 += $data->mix_money;
                $p_user->save();
                $NewsUser->content = '您的提現申請匹配餘額買家成功，請到提現記錄查看。Your exchange request matches the balance buyer，s success, please go to the check out record';
                UserModel::money_change($p_user->id,1,$data->mix_money,'提現申請 Apply for an application');
            }
            if ($info['status'] == 2) {
                $user = UserModel::get($data->user_id);
                $user->money1 += $data->mix_money;
                $user->save();
                $NewsUser->content = '您的提現申請匹配餘額買家失敗，請到提現記錄查看。Your exchange request matches the balance buyer，s failure, please check it out of the record';
                UserModel::money_change($user->id,1,$data->mix_money,'提現申請 Apply for an application');
            }
            $NewsUser->title = '提現信息。Exchange information';
            $NewsUser->save();
            $data->status = $info['status'];
            $data->save();
            $this->success(@$_SESSION['is_language'] ? '操作成功' : 'Successful operation', '', '', 1);
        }
        $this->assign([
            'data' => $data,
        ]);
        return $this->fetch();
    }


    /**
     * 兑出记录列表
     * @author
     */
    public function witndrawlist()
    {
        if (input('username')) {
            $user = UserModel::get(['username' => input('username')]);
            if (!$user) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        }
        isset($user) ? $map['user_id'] = $user->id : $map['user_id'] = session('home_user')['id'];
        $MixOutModel = new MixOutModel();
        $data = $MixOutModel->get_list($map, 'id desc', 15);
        $this->assign([
            'data' => $data,
            'user' => @$user
        ]);
        return $this->fetch();
    }

    /**
     * 兑出记录详情
     * @author
     */
    public function withdrawdetails()
    {
        $id = input('id');
        if (!$id) $this->error(@$_SESSION['is_language'] ? '缺少參數' : 'Missing parameters');
        MixOutModel::where('id', input('id'))->update(['is_see' => '1']);
        $MixOutModel = new MixOutModel();
        $data = $MixOutModel->get_find(['world_mix_out.id' => $id]);
        $this->assign([
            'data' => $data,
        ]);
        return $this->fetch();

    }


}