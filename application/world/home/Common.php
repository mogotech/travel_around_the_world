<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：Finance.php
 * 时间：2017年8月25日
 * 作者：
 */

namespace app\world\home;

use app\index\controller\Home;
use app\world\home\Base;
use app\world\model\User;

/**
 * 前台公共控制器
 * @package app\cms\admin
 */
class Common extends Base
{
    /**
     * 初始化方法
     */
    protected function _initialize()
    {
        parent::_initialize();
        isset($_SESSION['dolphin_admin_']['home_user']) ? '' : $this->error(@$_SESSION['is_language'] ? '請登錄' : 'Please login', url('login/index'));
        $user_id = @session('home_user')['id'];
        if (@$user_id) {
            $user = User::find($user_id)->toArray();
            session('home_user', $user);
        }
    }


}