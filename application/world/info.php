<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：info.php
 * 时间：2017年8月25日
 * 作者：
 */

/**
 * Created by PhpStorm.
 * User:
 * Date: 2017年8月25日
 * Time: 15点01分
 */


/**
 * 模块信息
 */
return [
    // 模块名[必填]
    'name' => 'world',
    // 模块标题[必填]
    'title' => 'HGB',
    // 模块唯一标识[必填]，格式：模块名.开发者标识.module
    'identifier' => 'world..module',
    // 模块图标[选填]
    'icon' => 'fa fa-fw fa-newspaper-o',
    // 模块描述[选填]
    'description' => 'HGB',
    // 开发者[必填]
    'author' => '',

    // 版本[必填],格式采用三段式：主版本号.次版本号.修订版本号
    'version' => '1.0.0',

    // 数据表[有数据库表时必填]
    'tables' => [

    ],

    'database_prefix' => 'dp_',

];
