<?php
/**
 * Copyrigh () 2017 湖南魔工坊科技有限公司 版权所有
 * 网址：http://www.mogo.club
 * 项目名称：环球世界交易平台
 * 文件名称：User.php
 * 时间：2017年8月25日
 * 作者：
 */


/**
 * 查询下级所有用户
 * @param $cate   需要晒寻的数组
 * @param $parent_id  最上级用户
 * @param bool $is_data 返回下级所有信息还是id
 * @return array
 */
function get_children_id($cate, $parent_id, $is_data = true,$dp=[])
{

    $arr = array();
    foreach (@(array)$cate as $v) {
        if ($v['pid'] == $parent_id) {
            if ($is_data) {
                $arr[] = $v;
            } else {
                $arr[] = $v['id'];
            }
            $arr = array_merge($arr, get_children_id($cate, $v['id'], $is_data));
        }
    }
    $dp['id'] = $arr;
    return $arr;
}

/**
 * 对下级用户数据序列化
 * @param $user     最上级用户
 * @param $alluser1 需要排序的用户
 */
function sdss(&$user, $alluser1)
{
    $sum = 0;
    $user['childhehe'] = array();
    $user['count'] = 0;

    foreach (@(array)$alluser1[$user['id']]['childhehe'] as $k => $val) {
        $num = count($alluser1[$user['id']]['childhehe']);
        $user['childhehe'][$sum] = $val;
        $user['count'] = $num;
        sdss($user['childhehe'][$sum], $alluser1);
        $sum++;
    }
    return $user;
}


/**
 * 查询上级所有用户
 * @param $cate //需要筛选的数组
 * @param $parent_id //上级id
 *  * @param bool $is_data 返回上级所有信息还是id
 * @return array
 */
function get_parent_id($cate, $parent_id, $is_data = true)
{
    $arr = array();
    global $i;
    foreach ($cate as $v) {
        if ($v['id'] == $parent_id) {
            $i++;
            $v['levels'] = $i;
            if ($is_data) {
                $arr[] = $v;
            } else {
                $arr[] = $v['id'];
            }
            $arr = array_merge($arr, get_parent_id($cate, $v['pid'], $is_data));
        }
    }
    return $arr;
}


/**
 * 序列化返回参数
 * @param $code
 * @param $msg
 * @param $content
 * @return array
 */
function code_return($code = 1, $msg = '', $content = '')
{
    return ['code' => $code, 'msg' => $msg, 'content' => $content];
}


//计算小数点后位数
function getFloatLength($num)
{
    $count = 0;
    $temp = explode('.', $num);
    if (sizeof($temp) > 1) {
        $decimal = end($temp);
        $count = strlen($decimal);
    }
    return $count;
}


/**
 * 二维数组排序
 */
function my_sort($arrays, $sort_key, $sort_order = SORT_ASC, $sort_type = SORT_NUMERIC)
{
    if (is_array($arrays)) {
        foreach ($arrays as $array) {
            if (is_array($array)) {
                $key_arrays[] = $array[$sort_key];
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
    array_multisort($key_arrays, $sort_order, $sort_type, $arrays);
    return $arrays;
}


/**
 * 数字格式化
 */
function num_format($num){
    if(!is_numeric($num)){
        return false;
    }
    $num = explode('.',$num);//把整数和小数分开
    $rl = $num[1];//小数部分的值
    $j = strlen($num[0]) % 3;//整数有多少位
    $sl = substr($num[0], 0, $j);//前面不满三位的数取出来
    $sr = substr($num[0], $j);//后面的满三位的数取出来
    $i = 0;
    while($i <= strlen($sr)){
        $rvalue = @$rvalue.','.substr($sr, $i, 3);//三位三位取出再合并，按逗号隔开
        $i = $i + 3;
    }
    $rvalue = $sl.$rvalue;
    $rvalue = substr($rvalue,0,strlen($rvalue)-1);//去掉最后一个逗号
    $rvalue = explode(',',$rvalue);//分解成数组
    if($rvalue[0]==0){
        array_shift($rvalue);//如果第一个元素为0，删除第一个元素
    }
    $rv = $rvalue[0];//前面不满三位的数
    for($i = 1; $i < count($rvalue); $i++){
        $rv = $rv.','.$rvalue[$i];
    }
    if(!empty($rl)){
        $rvalue = $rv.'.'.$rl;//小数不为空，整数和小数合并
    }else{
        $rvalue = $rv;//小数为空，只有整数
    }
    return $rvalue;
}





//得到随机小数
function get_rand($mix_num,$max_num){

    $FloatLength = getFloatLength($mix_num);
    $FloatLength=2;
    $jishu_rand = pow(10, $FloatLength);


    $shop_min = $mix_num * $jishu_rand;

    $shop_max = $max_num * $jishu_rand;

    $rand = rand($shop_min, $shop_max) / $jishu_rand;

    return $rand;
}








